// lladoware_pins.h
// 
// Onboard LED control for TI TM4C123GXL using Keil v5
// A simple library that initializes and updates all GPIO and PWM output pins 
// on a TI TM4C123GXL microcontroller
// 
// This file is part of lladoware v1.0
// Travis Llado, travis@travisllado.com
// Last modified 2020-04-22

////////////////////////////////////////////////////////////////////////////////
// Constants

// PWM Configuration
#define PWM_PERIOD_MAX      65535   // Defined by 16b register
#define PWM_FREQ_DEFAULT    1000    // Hz
#define PWM_PERIOD_DEFAULT  (SYS_FREQ/PWM_DIV/PWM_FREQ_DEFAULT)

// Onboard LED Configuration
#define LED_FREQ_DEFAULT        1000    // Hz
#define LED_PERIOD_DEFAULT      (SYS_FREQ/PWM_DIV/LED_FREQ_DEFAULT)
#define LED_DUTY_MAX_DEFAULT    (LED_PERIOD_DEFAULT-1)

// Pin Directions
#define INPUT   0x01    // arbitrary but unique number
#define OUTPUT  0x02    // arbitrary but unique number

// GPIO States
#define HIGH    0x00070001  // arbitrary but unique number greater than AI range
#define LOW     0x00070000  // arbitrary but unique number greater than AI range

// Pin Indices
#define PA2 0
#define PA3 1
#define PA4 2
#define PA5 3
#define PA6 4
#define PA7 5
#define PB0 6
#define PB1 7
#define PB2 8
#define PB3 9
#define PB4 10
#define PB5 11
#define PB6 12
#define PB7 13
#define PC4 14
#define PC5 15
#define PC6 16
#define PC7 17
#define PD0 18
#define PD1 19
#define PD2 20
#define PD3 21
#define PD6 22
#define PD7 23
#define PE0 24
#define PE1 25
#define PE2 26
#define PE3 27
#define PE4 28
#define PE5 29
#define PF0 30
#define PF1 31
#define PF2 32
#define PF3 33
#define PF4 34

#define BUTTON_LEFT     PF4
#define BUTTON_RIGHT    PF0

// Button Configuration
#define DEBOUNCE_LENGTH 500 // us

////////////////////////////////////////////////////////////////////////////////
// External Prototypes

////////////////////////////////////////////////////////////////////////////////
// Input Pin Control Functions

uint32_t init_button(uint32_t pin_index, uint32_t priority, void (*task)(void));
uint32_t init_pin_input_gpio(uint32_t pin_index);
uint32_t init_quad_encoder(uint32_t pin_index_a, uint32_t pin_index_b, uint32_t priority, void (*task)(void));
uint32_t read_pin(uint32_t pin_index);

////////////////////////////////////////////////////////////////////////////////
// Output Pin Control Functions

uint32_t init_pin_output(uint32_t pin_index, uint32_t period);
uint32_t update_pin(uint32_t pin_index, uint32_t command);

////////////////////////////////////////////////////////////////////////////////
// onboard LED control functions

void onboard_leds_init(void);
void onboard_leds_red_update(uint32_t command);
void onboard_leds_blue_update(uint32_t command);
void onboard_leds_green_update(uint32_t command);

////////////////////////////////////////////////////////////////////////////////
// End of file

















