// dsp.h
// 
// Car audio control interface for TI TM4C123GXL using Keil v5
// Program generates analog voltage control signals for Audiotec Fischer DSP 
// and modulates those voltages based on button input from user. Eventually 
// button input from user will be replaced with CANBUS input from car.
// 
// This file contains code that manages the operation of the PWM outputs that 
// drive indicator LEDs and analog voltage outputs.
// 
// This file is part of car_audio_interface v0.1
// Travis Llado, travis@travisllado.com
// Last modified 2020-04-28

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "lladoware_system_config.h"

////////////////////////////////////////////////////////////////////////////////
// Constants

#define DEBUG_UART  0

#define BASS_INIT   12
#define BASS_MIN    0
#define BASS_MAX    31
#define VOLUME_INIT 6
#define VOLUME_MIN  0
#define VOLUME_MAX  31

#define PIN_VOL_SGNL    PD1
#define PIN_BASS_SGNL   PD0

#define PIN_BASS_ENC_A  PA3
#define PIN_BASS_ENC_B  PA2
#define PIN_BASS_RESET  PA4

#define PWM_FREQ    10000   // Hz (Must be >> 1kHz due to RC filter time constant of 1ms)
#define PWM_PERIOD  SYS_FREQ/PWM_DIV/PWM_FREQ

////////////////////////////////////////////////////////////////////////////////
// dsp_init()
// Initializes PWM signals and channel levels

void dsp_init(void);

////////////////////////////////////////////////////////////////////////////////
// level change functions
// Raise or lower respective level by one tick

void dsp_bass_lower(void);
void dsp_bass_raise(void);
void dsp_bass_reset(void);
void dsp_volume_lower(void);
void dsp_volume_lower_by(uint32_t num_ticks);
void dsp_volume_raise(void);
void dsp_volume_raise_by(uint32_t num_ticks);
void dsp_volume_reset(void);

////////////////////////////////////////////////////////////////////////////////
// End of file
