// lladoware_pin_regs.c
// 
// This file contains a bunch of constant initializations that allow us to 
// access registers as array elements instead of as individual variables. This 
// makes our code much more maintainable.
// 
// This file is part of lladoware v1.0
// Travis Llado, travis@travisllado.com
// Last modified 2020-04-22

////////////////////////////////////////////////////////////////////////////////
// Constants

#define NUM_PINS 35

#define PORT_A  0x40004000  // GPIO_PORTA_DATA_BITS_R
#define PORT_B  0x40005000  // GPIO_PORTB_DATA_BITS_R
#define PORT_C  0x40006000  // GPIO_PORTC_DATA_BITS_R
#define PORT_D  0x40007000  // GPIO_PORTD_DATA_BITS_R
#define PORT_E  0x40024000  // GPIO_PORTE_DATA_BITS_R
#define PORT_F  0x40025000  // GPIO_PORTF_DATA_BITS_R

#define PIN_0   0x01<<(2+0)
#define PIN_1   0x01<<(2+1)
#define PIN_2   0x01<<(2+2)
#define PIN_3   0x01<<(2+3)
#define PIN_4   0x01<<(2+4)
#define PIN_5   0x01<<(2+5)
#define PIN_6   0x01<<(2+6)
#define PIN_7   0x01<<(2+7)

typedef volatile uint32_t* register_type;

#define INVALID_REG (*((volatile uint32_t *)0))
#define INVALID     0

// All of the following arrays are arranged in the form:
// 
//  n/a n/a PA2 PA3 PA4 PA5 PA6 PA7
//  PB0 PB1 PB2 PB3 PB4 PB5 PB6 PB7
//  n/a n/a n/a n/a PC4 PC5 PC6 PC7
//  PD0 PD1 PD2 PD3 n/a n/a PD6 PD7
//  PE0 PE1 PE2 PE3 PE4 PE5 n/a n/a
//  PF0 PF1 PF2 PF3 PF4 n/a n/a n/a
// 
// It's not very readable, but hopefully no one needs to read this.

const register_type GPIO_AFSEL_REG[NUM_PINS] = {
                                              &GPIO_PORTA_AFSEL_R, &GPIO_PORTA_AFSEL_R, &GPIO_PORTA_AFSEL_R, &GPIO_PORTA_AFSEL_R, &GPIO_PORTA_AFSEL_R, &GPIO_PORTA_AFSEL_R,
    &GPIO_PORTB_AFSEL_R, &GPIO_PORTB_AFSEL_R, &GPIO_PORTB_AFSEL_R, &GPIO_PORTB_AFSEL_R, &GPIO_PORTB_AFSEL_R, &GPIO_PORTB_AFSEL_R, &GPIO_PORTB_AFSEL_R, &GPIO_PORTB_AFSEL_R,
                                                                                        &GPIO_PORTC_AFSEL_R, &GPIO_PORTC_AFSEL_R, &GPIO_PORTC_AFSEL_R, &GPIO_PORTC_AFSEL_R,
    &GPIO_PORTD_AFSEL_R, &GPIO_PORTD_AFSEL_R, &GPIO_PORTD_AFSEL_R, &GPIO_PORTD_AFSEL_R,                                           &GPIO_PORTD_AFSEL_R, &GPIO_PORTD_AFSEL_R,
    &GPIO_PORTE_AFSEL_R, &GPIO_PORTE_AFSEL_R, &GPIO_PORTE_AFSEL_R, &GPIO_PORTE_AFSEL_R, &GPIO_PORTE_AFSEL_R, &GPIO_PORTE_AFSEL_R,
    &GPIO_PORTF_AFSEL_R, &GPIO_PORTF_AFSEL_R, &GPIO_PORTF_AFSEL_R, &GPIO_PORTF_AFSEL_R, &GPIO_PORTF_AFSEL_R
};

const register_type GPIO_AMSEL_REG[NUM_PINS] = {
                                              &GPIO_PORTA_AMSEL_R, &GPIO_PORTA_AMSEL_R, &GPIO_PORTA_AMSEL_R, &GPIO_PORTA_AMSEL_R, &GPIO_PORTA_AMSEL_R, &GPIO_PORTA_AMSEL_R,
    &GPIO_PORTB_AMSEL_R, &GPIO_PORTB_AMSEL_R, &GPIO_PORTB_AMSEL_R, &GPIO_PORTB_AMSEL_R, &GPIO_PORTB_AMSEL_R, &GPIO_PORTB_AMSEL_R, &GPIO_PORTB_AMSEL_R, &GPIO_PORTB_AMSEL_R,
                                                                                        &GPIO_PORTC_AMSEL_R, &GPIO_PORTC_AMSEL_R, &GPIO_PORTC_AMSEL_R, &GPIO_PORTC_AMSEL_R,
    &GPIO_PORTD_AMSEL_R, &GPIO_PORTD_AMSEL_R, &GPIO_PORTD_AMSEL_R, &GPIO_PORTD_AMSEL_R,                                           &GPIO_PORTD_AMSEL_R, &GPIO_PORTD_AMSEL_R,
    &GPIO_PORTE_AMSEL_R, &GPIO_PORTE_AMSEL_R, &GPIO_PORTE_AMSEL_R, &GPIO_PORTE_AMSEL_R, &GPIO_PORTE_AMSEL_R, &GPIO_PORTE_AMSEL_R,
    &GPIO_PORTF_AMSEL_R, &GPIO_PORTF_AMSEL_R, &GPIO_PORTF_AMSEL_R, &GPIO_PORTF_AMSEL_R, &GPIO_PORTF_AMSEL_R
};

const register_type GPIO_CR_REG[NUM_PINS] = {
                                            &GPIO_PORTA_CR_R,   &GPIO_PORTA_CR_R,   &GPIO_PORTA_CR_R,   &GPIO_PORTA_CR_R,   &GPIO_PORTA_CR_R,   &GPIO_PORTA_CR_R,
    &GPIO_PORTB_CR_R,   &GPIO_PORTB_CR_R,   &GPIO_PORTB_CR_R,   &GPIO_PORTB_CR_R,   &GPIO_PORTB_CR_R,   &GPIO_PORTB_CR_R,   &GPIO_PORTB_CR_R,   &GPIO_PORTB_CR_R,
                                                                                    &GPIO_PORTC_CR_R,   &GPIO_PORTC_CR_R,   &GPIO_PORTC_CR_R,   &GPIO_PORTC_CR_R,
    &GPIO_PORTD_CR_R,   &GPIO_PORTD_CR_R,   &GPIO_PORTD_CR_R,   &GPIO_PORTD_CR_R,                                           &GPIO_PORTD_CR_R,   &GPIO_PORTD_CR_R,
    &GPIO_PORTE_CR_R,   &GPIO_PORTE_CR_R,   &GPIO_PORTE_CR_R,   &GPIO_PORTE_CR_R,   &GPIO_PORTE_CR_R,   &GPIO_PORTE_CR_R,
    &GPIO_PORTF_CR_R,   &GPIO_PORTF_CR_R,   &GPIO_PORTF_CR_R,   &GPIO_PORTF_CR_R,   &GPIO_PORTF_CR_R
};

const register_type GPIO_DATA_BITS_REG[NUM_PINS] = {
                                                                                (volatile uint32_t*)(PORT_A | PIN_2), (volatile uint32_t*)(PORT_A | PIN_3), (volatile uint32_t*)(PORT_A | PIN_4), (volatile uint32_t*)(PORT_A | PIN_5), (volatile uint32_t*)(PORT_A | PIN_6), (volatile uint32_t*)(PORT_A | PIN_7),
    (volatile uint32_t*)(PORT_B | PIN_0), (volatile uint32_t*)(PORT_B | PIN_1), (volatile uint32_t*)(PORT_B | PIN_2), (volatile uint32_t*)(PORT_B | PIN_3), (volatile uint32_t*)(PORT_B | PIN_4), (volatile uint32_t*)(PORT_B | PIN_5), (volatile uint32_t*)(PORT_B | PIN_6), (volatile uint32_t*)(PORT_B | PIN_7),
                                                                                                                                                            (volatile uint32_t*)(PORT_C | PIN_4), (volatile uint32_t*)(PORT_C | PIN_5), (volatile uint32_t*)(PORT_C | PIN_6), (volatile uint32_t*)(PORT_C | PIN_7),
    (volatile uint32_t*)(PORT_D | PIN_0), (volatile uint32_t*)(PORT_D | PIN_1), (volatile uint32_t*)(PORT_D | PIN_2), (volatile uint32_t*)(PORT_D | PIN_3),                                                                             (volatile uint32_t*)(PORT_D | PIN_6), (volatile uint32_t*)(PORT_D | PIN_7),
    (volatile uint32_t*)(PORT_E | PIN_0), (volatile uint32_t*)(PORT_E | PIN_1), (volatile uint32_t*)(PORT_E | PIN_2), (volatile uint32_t*)(PORT_E | PIN_3), (volatile uint32_t*)(PORT_E | PIN_4), (volatile uint32_t*)(PORT_E | PIN_5),
    (volatile uint32_t*)(PORT_F | PIN_0), (volatile uint32_t*)(PORT_F | PIN_1), (volatile uint32_t*)(PORT_F | PIN_2), (volatile uint32_t*)(PORT_F | PIN_3), (volatile uint32_t*)(PORT_F | PIN_4)
};

const register_type GPIO_DATA_REG[NUM_PINS] = {
                                            &GPIO_PORTA_DATA_R, &GPIO_PORTA_DATA_R, &GPIO_PORTA_DATA_R, &GPIO_PORTA_DATA_R, &GPIO_PORTA_DATA_R,   &GPIO_PORTA_DATA_R,
    &GPIO_PORTB_DATA_R, &GPIO_PORTB_DATA_R, &GPIO_PORTB_DATA_R, &GPIO_PORTB_DATA_R, &GPIO_PORTB_DATA_R, &GPIO_PORTB_DATA_R, &GPIO_PORTB_DATA_R,   &GPIO_PORTB_DATA_R,
                                                                                    &GPIO_PORTC_DATA_R, &GPIO_PORTC_DATA_R, &GPIO_PORTC_DATA_R,   &GPIO_PORTC_DATA_R,
    &GPIO_PORTD_DATA_R, &GPIO_PORTD_DATA_R, &GPIO_PORTD_DATA_R, &GPIO_PORTD_DATA_R,                                         &GPIO_PORTD_DATA_R,   &GPIO_PORTD_DATA_R,
    &GPIO_PORTE_DATA_R, &GPIO_PORTE_DATA_R, &GPIO_PORTE_DATA_R, &GPIO_PORTE_DATA_R, &GPIO_PORTE_DATA_R, &GPIO_PORTE_DATA_R,
    &GPIO_PORTF_DATA_R, &GPIO_PORTF_DATA_R, &GPIO_PORTF_DATA_R, &GPIO_PORTF_DATA_R, &GPIO_PORTF_DATA_R
};

const register_type GPIO_DEN_REG[NUM_PINS] = {
                                          &GPIO_PORTA_DEN_R, &GPIO_PORTA_DEN_R, &GPIO_PORTA_DEN_R, &GPIO_PORTA_DEN_R, &GPIO_PORTA_DEN_R, &GPIO_PORTA_DEN_R,
    &GPIO_PORTB_DEN_R, &GPIO_PORTB_DEN_R, &GPIO_PORTB_DEN_R, &GPIO_PORTB_DEN_R, &GPIO_PORTB_DEN_R, &GPIO_PORTB_DEN_R, &GPIO_PORTB_DEN_R, &GPIO_PORTB_DEN_R,
                                                                                &GPIO_PORTC_DEN_R, &GPIO_PORTC_DEN_R, &GPIO_PORTC_DEN_R, &GPIO_PORTC_DEN_R,
    &GPIO_PORTD_DEN_R, &GPIO_PORTD_DEN_R, &GPIO_PORTD_DEN_R, &GPIO_PORTD_DEN_R,                                       &GPIO_PORTD_DEN_R, &GPIO_PORTD_DEN_R,
    &GPIO_PORTE_DEN_R, &GPIO_PORTE_DEN_R, &GPIO_PORTE_DEN_R, &GPIO_PORTE_DEN_R, &GPIO_PORTE_DEN_R, &GPIO_PORTE_DEN_R,
    &GPIO_PORTF_DEN_R, &GPIO_PORTF_DEN_R, &GPIO_PORTF_DEN_R, &GPIO_PORTF_DEN_R, &GPIO_PORTF_DEN_R
};

const register_type GPIO_DIR_REG[NUM_PINS] = {
                                          &GPIO_PORTA_DIR_R, &GPIO_PORTA_DIR_R, &GPIO_PORTA_DIR_R, &GPIO_PORTA_DIR_R, &GPIO_PORTA_DIR_R, &GPIO_PORTA_DIR_R,
    &GPIO_PORTB_DIR_R, &GPIO_PORTB_DIR_R, &GPIO_PORTB_DIR_R, &GPIO_PORTB_DIR_R, &GPIO_PORTB_DIR_R, &GPIO_PORTB_DIR_R, &GPIO_PORTB_DIR_R, &GPIO_PORTB_DIR_R,
                                                                                &GPIO_PORTC_DIR_R, &GPIO_PORTC_DIR_R, &GPIO_PORTC_DIR_R, &GPIO_PORTC_DIR_R,
    &GPIO_PORTD_DIR_R, &GPIO_PORTD_DIR_R, &GPIO_PORTD_DIR_R, &GPIO_PORTD_DIR_R,                                       &GPIO_PORTD_DIR_R, &GPIO_PORTD_DIR_R,
    &GPIO_PORTE_DIR_R, &GPIO_PORTE_DIR_R, &GPIO_PORTE_DIR_R, &GPIO_PORTE_DIR_R, &GPIO_PORTE_DIR_R, &GPIO_PORTE_DIR_R,
    &GPIO_PORTF_DIR_R, &GPIO_PORTF_DIR_R, &GPIO_PORTF_DIR_R, &GPIO_PORTF_DIR_R, &GPIO_PORTF_DIR_R
};

const register_type GPIO_IBE_REG[NUM_PINS] = {
                                            &GPIO_PORTA_IBE_R,  &GPIO_PORTA_IBE_R,  &GPIO_PORTA_IBE_R,  &GPIO_PORTA_IBE_R,  &GPIO_PORTA_IBE_R,  &GPIO_PORTA_IBE_R,
    &GPIO_PORTB_IBE_R,  &GPIO_PORTB_IBE_R,  &GPIO_PORTB_IBE_R,  &GPIO_PORTB_IBE_R,  &GPIO_PORTB_IBE_R,  &GPIO_PORTB_IBE_R,  &GPIO_PORTB_IBE_R,  &GPIO_PORTB_IBE_R,
                                                                                    &GPIO_PORTC_IBE_R,  &GPIO_PORTC_IBE_R,  &GPIO_PORTC_IBE_R,  &GPIO_PORTC_IBE_R,
    &GPIO_PORTD_IBE_R,  &GPIO_PORTD_IBE_R,  &GPIO_PORTD_IBE_R,  &GPIO_PORTD_IBE_R,                                          &GPIO_PORTD_IBE_R,  &GPIO_PORTD_IBE_R,
    &GPIO_PORTE_IBE_R,  &GPIO_PORTE_IBE_R,  &GPIO_PORTE_IBE_R,  &GPIO_PORTE_IBE_R,  &GPIO_PORTE_IBE_R,  &GPIO_PORTE_IBE_R,
    &GPIO_PORTF_IBE_R,  &GPIO_PORTF_IBE_R,  &GPIO_PORTF_IBE_R,  &GPIO_PORTF_IBE_R,  &GPIO_PORTF_IBE_R
};

const register_type GPIO_ICR_REG[NUM_PINS] = {
                                            &GPIO_PORTA_ICR_R,  &GPIO_PORTA_ICR_R,  &GPIO_PORTA_ICR_R,  &GPIO_PORTA_ICR_R,  &GPIO_PORTA_ICR_R,  &GPIO_PORTA_ICR_R,
    &GPIO_PORTB_ICR_R,  &GPIO_PORTB_ICR_R,  &GPIO_PORTB_ICR_R,  &GPIO_PORTB_ICR_R,  &GPIO_PORTB_ICR_R,  &GPIO_PORTB_ICR_R,  &GPIO_PORTB_ICR_R,  &GPIO_PORTB_ICR_R,
                                                                                    &GPIO_PORTC_ICR_R,  &GPIO_PORTC_ICR_R,  &GPIO_PORTC_ICR_R,  &GPIO_PORTC_ICR_R,
    &GPIO_PORTD_ICR_R,  &GPIO_PORTD_ICR_R,  &GPIO_PORTD_ICR_R,  &GPIO_PORTD_ICR_R,                                          &GPIO_PORTD_ICR_R,  &GPIO_PORTD_ICR_R,
    &GPIO_PORTE_ICR_R,  &GPIO_PORTE_ICR_R,  &GPIO_PORTE_ICR_R,  &GPIO_PORTE_ICR_R,  &GPIO_PORTE_ICR_R,  &GPIO_PORTE_ICR_R,
    &GPIO_PORTF_ICR_R,  &GPIO_PORTF_ICR_R,  &GPIO_PORTF_ICR_R,  &GPIO_PORTF_ICR_R,  &GPIO_PORTF_ICR_R
};

const register_type GPIO_IEV_REG[NUM_PINS] = {
                                            &GPIO_PORTA_IEV_R,  &GPIO_PORTA_IEV_R,  &GPIO_PORTA_IEV_R,  &GPIO_PORTA_IEV_R,  &GPIO_PORTA_IEV_R,  &GPIO_PORTA_IEV_R,
    &GPIO_PORTB_IEV_R,  &GPIO_PORTB_IEV_R,  &GPIO_PORTB_IEV_R,  &GPIO_PORTB_IEV_R,  &GPIO_PORTB_IEV_R,  &GPIO_PORTB_IEV_R,  &GPIO_PORTB_IEV_R,  &GPIO_PORTB_IEV_R,
                                                                                    &GPIO_PORTC_IEV_R,  &GPIO_PORTC_IEV_R,  &GPIO_PORTC_IEV_R,  &GPIO_PORTC_IEV_R,
    &GPIO_PORTD_IEV_R,  &GPIO_PORTD_IEV_R,  &GPIO_PORTD_IEV_R,  &GPIO_PORTD_IEV_R,                                          &GPIO_PORTD_IEV_R,  &GPIO_PORTD_IEV_R,
    &GPIO_PORTE_IEV_R,  &GPIO_PORTE_IEV_R,  &GPIO_PORTE_IEV_R,  &GPIO_PORTE_IEV_R,  &GPIO_PORTE_IEV_R,  &GPIO_PORTE_IEV_R,
    &GPIO_PORTF_IEV_R,  &GPIO_PORTF_IEV_R,  &GPIO_PORTF_IEV_R,  &GPIO_PORTF_IEV_R,  &GPIO_PORTF_IEV_R
};

const register_type GPIO_IM_REG[NUM_PINS] = {
                                            &GPIO_PORTA_IM_R,  &GPIO_PORTA_IM_R,    &GPIO_PORTA_IM_R,   &GPIO_PORTA_IM_R,   &GPIO_PORTA_IM_R,   &GPIO_PORTA_IM_R,
    &GPIO_PORTB_IM_R,   &GPIO_PORTB_IM_R,   &GPIO_PORTB_IM_R,  &GPIO_PORTB_IM_R,    &GPIO_PORTB_IM_R,   &GPIO_PORTB_IM_R,   &GPIO_PORTB_IM_R,   &GPIO_PORTB_IM_R,
                                                                                    &GPIO_PORTC_IM_R,   &GPIO_PORTC_IM_R,   &GPIO_PORTC_IM_R,   &GPIO_PORTC_IM_R,
    &GPIO_PORTD_IM_R,   &GPIO_PORTD_IM_R,   &GPIO_PORTD_IM_R,  &GPIO_PORTD_IM_R,                                            &GPIO_PORTD_IM_R,   &GPIO_PORTD_IM_R,
    &GPIO_PORTE_IM_R,   &GPIO_PORTE_IM_R,   &GPIO_PORTE_IM_R,  &GPIO_PORTE_IM_R,    &GPIO_PORTE_IM_R,   &GPIO_PORTE_IM_R,
    &GPIO_PORTF_IM_R,   &GPIO_PORTF_IM_R,   &GPIO_PORTF_IM_R,  &GPIO_PORTF_IM_R,    &GPIO_PORTF_IM_R
};

const register_type GPIO_IS_REG[NUM_PINS] = {
                                            &GPIO_PORTA_IS_R,  &GPIO_PORTA_IS_R,    &GPIO_PORTA_IS_R,   &GPIO_PORTA_IS_R,   &GPIO_PORTA_IS_R,   &GPIO_PORTA_IS_R,
    &GPIO_PORTB_IS_R,   &GPIO_PORTB_IS_R,   &GPIO_PORTB_IS_R,  &GPIO_PORTB_IS_R,    &GPIO_PORTB_IS_R,   &GPIO_PORTB_IS_R,   &GPIO_PORTB_IS_R,   &GPIO_PORTB_IS_R,
                                                                                    &GPIO_PORTC_IS_R,   &GPIO_PORTC_IS_R,   &GPIO_PORTC_IS_R,   &GPIO_PORTC_IS_R,
    &GPIO_PORTD_IS_R,   &GPIO_PORTD_IS_R,   &GPIO_PORTD_IS_R,  &GPIO_PORTD_IS_R,                                            &GPIO_PORTD_IS_R,   &GPIO_PORTD_IS_R,
    &GPIO_PORTE_IS_R,   &GPIO_PORTE_IS_R,   &GPIO_PORTE_IS_R,  &GPIO_PORTE_IS_R,    &GPIO_PORTE_IS_R,   &GPIO_PORTE_IS_R,
    &GPIO_PORTF_IS_R,   &GPIO_PORTF_IS_R,   &GPIO_PORTF_IS_R,  &GPIO_PORTF_IS_R,    &GPIO_PORTF_IS_R
};

const register_type GPIO_LOCK_REG[NUM_PINS] = {
                                            &GPIO_PORTA_LOCK_R, &GPIO_PORTA_LOCK_R, &GPIO_PORTA_LOCK_R, &GPIO_PORTA_LOCK_R, &GPIO_PORTA_LOCK_R, &GPIO_PORTA_LOCK_R,
    &GPIO_PORTB_LOCK_R, &GPIO_PORTB_LOCK_R, &GPIO_PORTB_LOCK_R, &GPIO_PORTB_LOCK_R, &GPIO_PORTB_LOCK_R, &GPIO_PORTB_LOCK_R, &GPIO_PORTB_LOCK_R, &GPIO_PORTB_LOCK_R,
                                                                                    &GPIO_PORTC_LOCK_R, &GPIO_PORTC_LOCK_R, &GPIO_PORTC_LOCK_R, &GPIO_PORTC_LOCK_R,
    &GPIO_PORTD_LOCK_R, &GPIO_PORTD_LOCK_R, &GPIO_PORTD_LOCK_R, &GPIO_PORTD_LOCK_R,                                         &GPIO_PORTD_LOCK_R, &GPIO_PORTD_LOCK_R,
    &GPIO_PORTE_LOCK_R, &GPIO_PORTE_LOCK_R, &GPIO_PORTE_LOCK_R, &GPIO_PORTE_LOCK_R, &GPIO_PORTE_LOCK_R, &GPIO_PORTE_LOCK_R,
    &GPIO_PORTF_LOCK_R, &GPIO_PORTF_LOCK_R, &GPIO_PORTF_LOCK_R, &GPIO_PORTF_LOCK_R, &GPIO_PORTF_LOCK_R
};

const register_type GPIO_PCTL_REG[NUM_PINS] = {
                                            &GPIO_PORTA_PCTL_R, &GPIO_PORTA_PCTL_R, &GPIO_PORTA_PCTL_R, &GPIO_PORTA_PCTL_R, &GPIO_PORTA_PCTL_R, &GPIO_PORTA_PCTL_R,
    &GPIO_PORTB_PCTL_R, &GPIO_PORTB_PCTL_R, &GPIO_PORTB_PCTL_R, &GPIO_PORTB_PCTL_R, &GPIO_PORTB_PCTL_R, &GPIO_PORTB_PCTL_R, &GPIO_PORTB_PCTL_R, &GPIO_PORTB_PCTL_R,
                                                                                    &GPIO_PORTC_PCTL_R, &GPIO_PORTC_PCTL_R, &GPIO_PORTC_PCTL_R, &GPIO_PORTC_PCTL_R,
    &GPIO_PORTD_PCTL_R, &GPIO_PORTD_PCTL_R, &GPIO_PORTD_PCTL_R, &GPIO_PORTD_PCTL_R,                                         &GPIO_PORTD_PCTL_R, &GPIO_PORTD_PCTL_R,
    &GPIO_PORTE_PCTL_R, &GPIO_PORTE_PCTL_R, &GPIO_PORTE_PCTL_R, &GPIO_PORTE_PCTL_R, &GPIO_PORTE_PCTL_R, &GPIO_PORTE_PCTL_R,
    &GPIO_PORTF_PCTL_R, &GPIO_PORTF_PCTL_R, &GPIO_PORTF_PCTL_R, &GPIO_PORTF_PCTL_R, &GPIO_PORTF_PCTL_R
};

const register_type GPIO_PUR_REG[NUM_PINS] = {
                                            &GPIO_PORTA_PUR_R,  &GPIO_PORTA_PUR_R,  &GPIO_PORTA_PUR_R,  &GPIO_PORTA_PUR_R,  &GPIO_PORTA_PUR_R,  &GPIO_PORTA_PUR_R,
    &GPIO_PORTB_PUR_R,  &GPIO_PORTB_PUR_R,  &GPIO_PORTB_PUR_R,  &GPIO_PORTB_PUR_R,  &GPIO_PORTB_PUR_R,  &GPIO_PORTB_PUR_R,  &GPIO_PORTB_PUR_R,  &GPIO_PORTB_PUR_R,
                                                                                    &GPIO_PORTC_PUR_R,  &GPIO_PORTC_PUR_R,  &GPIO_PORTC_PUR_R,  &GPIO_PORTC_PUR_R,
    &GPIO_PORTD_PUR_R,  &GPIO_PORTD_PUR_R,  &GPIO_PORTD_PUR_R,  &GPIO_PORTD_PUR_R,                                          &GPIO_PORTD_PUR_R,  &GPIO_PORTD_PUR_R,
    &GPIO_PORTE_PUR_R,  &GPIO_PORTE_PUR_R,  &GPIO_PORTE_PUR_R,  &GPIO_PORTE_PUR_R,  &GPIO_PORTE_PUR_R,  &GPIO_PORTE_PUR_R,
    &GPIO_PORTF_PUR_R,  &GPIO_PORTF_PUR_R,  &GPIO_PORTF_PUR_R,  &GPIO_PORTF_PUR_R,  &GPIO_PORTF_PUR_R
};

const register_type GPIO_RIS_REG[NUM_PINS] = {
                                            &GPIO_PORTA_RIS_R,  &GPIO_PORTA_RIS_R,  &GPIO_PORTA_RIS_R,  &GPIO_PORTA_RIS_R,  &GPIO_PORTA_RIS_R,  &GPIO_PORTA_RIS_R,
    &GPIO_PORTB_RIS_R,  &GPIO_PORTB_RIS_R,  &GPIO_PORTB_RIS_R,  &GPIO_PORTB_RIS_R,  &GPIO_PORTB_RIS_R,  &GPIO_PORTB_RIS_R,  &GPIO_PORTB_RIS_R,  &GPIO_PORTB_RIS_R,
                                                                                    &GPIO_PORTC_RIS_R,  &GPIO_PORTC_RIS_R,  &GPIO_PORTC_RIS_R,  &GPIO_PORTC_RIS_R,
    &GPIO_PORTD_RIS_R,  &GPIO_PORTD_RIS_R,  &GPIO_PORTD_RIS_R,  &GPIO_PORTD_RIS_R,                                          &GPIO_PORTD_RIS_R,  &GPIO_PORTD_RIS_R,
    &GPIO_PORTE_RIS_R,  &GPIO_PORTE_RIS_R,  &GPIO_PORTE_RIS_R,  &GPIO_PORTE_RIS_R,  &GPIO_PORTE_RIS_R,  &GPIO_PORTE_RIS_R,
    &GPIO_PORTF_RIS_R,  &GPIO_PORTF_RIS_R,  &GPIO_PORTF_RIS_R,  &GPIO_PORTF_RIS_R,  &GPIO_PORTF_RIS_R
};

const uint32_t NVIC_ENBL_REG[NUM_PINS] = {
                                    NVIC_EN0_INT0,  NVIC_EN0_INT0,  NVIC_EN0_INT0,  NVIC_EN0_INT0,  NVIC_EN0_INT0,  NVIC_EN0_INT0,
    NVIC_EN0_INT1,  NVIC_EN0_INT1,  NVIC_EN0_INT1,  NVIC_EN0_INT1,  NVIC_EN0_INT1,  NVIC_EN0_INT1,  NVIC_EN0_INT1,  NVIC_EN0_INT1,
                                                                    NVIC_EN0_INT2,  NVIC_EN0_INT2,  NVIC_EN0_INT2,  NVIC_EN0_INT2,
    NVIC_EN0_INT3,  NVIC_EN0_INT3,  NVIC_EN0_INT3,  NVIC_EN0_INT3,                                  NVIC_EN0_INT3,  NVIC_EN0_INT3,
    NVIC_EN0_INT4,  NVIC_EN0_INT4,  NVIC_EN0_INT4,  NVIC_EN0_INT4,  NVIC_EN0_INT4,  NVIC_EN0_INT4,
    NVIC_EN0_INT30, NVIC_EN0_INT30, NVIC_EN0_INT30, NVIC_EN0_INT30, NVIC_EN0_INT30
};

const uint32_t NVIC_PRIO_OFFSET[NUM_PINS] = {
             5,  5,  5,  5,  5,  5,
    13, 13, 13, 13, 13, 13, 13, 13,
                    21, 21, 21, 21,
    29, 29, 29, 29,         29, 29,
     5,  5,  5,  5,  5,  5,
    21, 21, 21, 21, 21
};

const register_type NVIC_PRIO_REG[NUM_PINS] = {
                                    &NVIC_PRI0_R,  &NVIC_PRI0_R,    &NVIC_PRI0_R,   &NVIC_PRI0_R,   &NVIC_PRI0_R,   &NVIC_PRI0_R,
    &NVIC_PRI0_R,   &NVIC_PRI0_R,   &NVIC_PRI0_R,  &NVIC_PRI0_R,    &NVIC_PRI0_R,   &NVIC_PRI0_R,   &NVIC_PRI0_R,   &NVIC_PRI0_R,
                                                                    &NVIC_PRI0_R,   &NVIC_PRI0_R,   &NVIC_PRI0_R,   &NVIC_PRI0_R,
    &NVIC_PRI0_R,   &NVIC_PRI0_R,   &NVIC_PRI0_R,  &NVIC_PRI0_R,                                    &NVIC_PRI0_R,   &NVIC_PRI0_R,
    &NVIC_PRI1_R,   &NVIC_PRI1_R,   &NVIC_PRI1_R,  &NVIC_PRI1_R,    &NVIC_PRI1_R,   &NVIC_PRI1_R,
    &NVIC_PRI7_R,   &NVIC_PRI7_R,   &NVIC_PRI7_R,  &NVIC_PRI7_R,    &NVIC_PRI7_R
};

const uint32_t PIN_NUM[NUM_PINS] = {
          2, 3, 4, 5, 6, 7,
    0, 1, 2, 3, 4, 5, 6, 7,
                4, 5, 6, 7,
    0, 1, 2, 3,       6, 7,
    0, 1, 2, 3, 4, 5,
    0, 1, 2, 3, 4
};

const uint32_t PWM_AF_NUM[NUM_PINS] = {
                      INVALID, INVALID, INVALID, INVALID, 5,       5,
    INVALID, INVALID, INVALID, INVALID, 4,       4,       4,       4,
                                        4,       4,       INVALID, INVALID,
    5,       5,       INVALID, INVALID,                   INVALID, INVALID,
    INVALID, INVALID, INVALID, INVALID, 4,4,
    5,       5,       5,       5,       INVALID
};

const uint32_t PWM_BLK_ENBL[NUM_PINS] = {
                                        INVALID,          INVALID,          INVALID,          INVALID,          PWM_1_CTL_ENABLE, PWM_1_CTL_ENABLE,
    INVALID,          INVALID,          INVALID,          INVALID,          PWM_1_CTL_ENABLE, PWM_1_CTL_ENABLE, PWM_0_CTL_ENABLE, PWM_0_CTL_ENABLE,
                                                                            PWM_3_CTL_ENABLE, PWM_3_CTL_ENABLE, INVALID,          INVALID,
    PWM_0_CTL_ENABLE, PWM_0_CTL_ENABLE, INVALID,          INVALID,                                              INVALID,          INVALID,
    INVALID,          INVALID,          INVALID,          INVALID,          PWM_2_CTL_ENABLE, PWM_2_CTL_ENABLE,
    PWM_2_CTL_ENABLE, PWM_2_CTL_ENABLE, PWM_3_CTL_ENABLE, PWM_3_CTL_ENABLE, INVALID
};

const register_type PWM_CMP_REG[NUM_PINS] = {
                                    &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &PWM1_1_CMPA_R, &PWM1_1_CMPB_R,
    &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &PWM0_1_CMPA_R, &PWM0_1_CMPB_R, &PWM0_0_CMPA_R, &PWM0_0_CMPB_R,
                                                                    &PWM0_3_CMPA_R, &PWM0_3_CMPB_R, &INVALID_REG,   &INVALID_REG,
    &PWM1_0_CMPA_R, &PWM1_0_CMPB_R, &INVALID_REG,   &INVALID_REG,                                   &INVALID_REG,   &INVALID_REG,
    &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &PWM0_2_CMPA_R, &PWM0_2_CMPB_R,
    &PWM1_2_CMPA_R, &PWM1_2_CMPB_R, &PWM1_3_CMPA_R, &PWM1_3_CMPB_R, &INVALID_REG
};

const register_type PWM_CTL_REG[NUM_PINS] = {
                                  &INVALID_REG,  &INVALID_REG,  &INVALID_REG,  &INVALID_REG,  &PWM1_1_CTL_R, &PWM1_1_CTL_R,
    &INVALID_REG,  &INVALID_REG,  &INVALID_REG,  &INVALID_REG,  &PWM0_1_CTL_R, &PWM0_1_CTL_R, &PWM0_0_CTL_R, &PWM0_0_CTL_R,
                                                                &PWM0_3_CTL_R, &PWM0_3_CTL_R, &INVALID_REG,  &INVALID_REG,
    &PWM1_0_CTL_R, &PWM1_0_CTL_R, &INVALID_REG,  &INVALID_REG,                                &INVALID_REG,  &INVALID_REG,
    &INVALID_REG,  &INVALID_REG,  &INVALID_REG,  &INVALID_REG,  &PWM0_2_CTL_R, &PWM0_2_CTL_R,
    &PWM1_2_CTL_R, &PWM1_2_CTL_R, &PWM1_3_CTL_R, &PWM1_3_CTL_R, &INVALID_REG
};

const register_type PWM_ENBL_REG[NUM_PINS] = {
                                    &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &PWM1_ENABLE_R, &PWM1_ENABLE_R,
    &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &PWM0_ENABLE_R, &PWM0_ENABLE_R, &PWM0_ENABLE_R, &PWM0_ENABLE_R,
                                                                    &PWM0_ENABLE_R, &PWM0_ENABLE_R, &INVALID_REG,   &INVALID_REG,
    &PWM1_ENABLE_R, &PWM1_ENABLE_R, &INVALID_REG,   &INVALID_REG,                                   &INVALID_REG,   &INVALID_REG,
    &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &PWM0_ENABLE_R, &PWM0_ENABLE_R,
    &PWM1_ENABLE_R, &PWM1_ENABLE_R, &PWM1_ENABLE_R, &PWM1_ENABLE_R, &INVALID_REG
};

const register_type PWM_GEN_REG[NUM_PINS] = {
                                    &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &PWM1_1_GENA_R, &PWM1_1_GENB_R,
    &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &PWM0_1_GENA_R, &PWM0_1_GENB_R, &PWM0_0_GENA_R, &PWM0_0_GENB_R,
                                                                    &PWM0_3_GENA_R, &PWM0_3_GENB_R, &INVALID_REG,   &INVALID_REG,
    &PWM1_0_GENA_R, &PWM1_0_GENB_R, &INVALID_REG,   &INVALID_REG,                                   &INVALID_REG,   &INVALID_REG,
    &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &PWM0_2_GENA_R, &PWM0_2_GENB_R,
    &PWM1_2_GENA_R, &PWM1_2_GENB_R, &PWM1_3_GENA_R, &PWM1_3_GENB_R, &INVALID_REG
};

const register_type PWM_LOAD_REG[NUM_PINS] = {
                                    &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &PWM1_1_LOAD_R,   &PWM1_1_LOAD_R,
    &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &PWM0_1_LOAD_R,   &PWM0_1_LOAD_R, &PWM0_0_LOAD_R, &PWM0_0_LOAD_R,
                                                                    &PWM0_3_LOAD_R, &PWM0_3_LOAD_R, &INVALID_REG,   &INVALID_REG,
    &PWM1_0_LOAD_R, &PWM1_0_LOAD_R, &INVALID_REG,   &INVALID_REG,                                   &INVALID_REG,   &INVALID_REG,
    &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &INVALID_REG,   &PWM0_2_LOAD_R, &PWM0_2_LOAD_R,
    &PWM1_2_LOAD_R, &PWM1_2_LOAD_R, &PWM1_3_LOAD_R, &PWM1_3_LOAD_R, &INVALID_REG
};

const uint32_t PWM_MOD_ENBL[NUM_PINS] = {
                                          INVALID,           INVALID,           INVALID,           INVALID,           PWM_ENABLE_PWM2EN, PWM_ENABLE_PWM3EN,
    INVALID,           INVALID,           INVALID,           INVALID,           PWM_ENABLE_PWM2EN, PWM_ENABLE_PWM3EN, PWM_ENABLE_PWM0EN, PWM_ENABLE_PWM1EN,
                                                                                PWM_ENABLE_PWM6EN, PWM_ENABLE_PWM7EN, INVALID,           INVALID,
    PWM_ENABLE_PWM0EN, PWM_ENABLE_PWM1EN, INVALID,           INVALID,                                                 INVALID,           INVALID,
    INVALID,           INVALID,           INVALID,           INVALID,           PWM_ENABLE_PWM4EN, PWM_ENABLE_PWM5EN,
    PWM_ENABLE_PWM4EN, PWM_ENABLE_PWM5EN, PWM_ENABLE_PWM6EN, PWM_ENABLE_PWM7EN, INVALID
};

const uint32_t PWM_TRGR[NUM_PINS] = {
                                                                                                              INVALID,                                             INVALID,                                             INVALID,                                             INVALID,                                             (PWM_1_GENA_ACTCMPAD_ONE | PWM_1_GENA_ACTLOAD_ZERO), (PWM_1_GENB_ACTCMPBD_ONE | PWM_1_GENB_ACTLOAD_ZERO),
    INVALID,                                             INVALID,                                             INVALID,                                             INVALID,                                             (PWM_1_GENA_ACTCMPAD_ONE | PWM_1_GENA_ACTLOAD_ZERO), (PWM_1_GENB_ACTCMPBD_ONE | PWM_1_GENB_ACTLOAD_ZERO), (PWM_0_GENA_ACTCMPAD_ONE | PWM_0_GENA_ACTLOAD_ZERO), (PWM_0_GENB_ACTCMPBD_ONE | PWM_0_GENB_ACTLOAD_ZERO),
                                                                                                                                                                                                                        (PWM_3_GENA_ACTCMPAD_ONE | PWM_3_GENA_ACTLOAD_ZERO), (PWM_3_GENB_ACTCMPBD_ONE | PWM_3_GENB_ACTLOAD_ZERO), INVALID,                                             INVALID,
    (PWM_0_GENA_ACTCMPAD_ONE | PWM_0_GENA_ACTLOAD_ZERO), (PWM_0_GENB_ACTCMPBD_ONE | PWM_0_GENB_ACTLOAD_ZERO), INVALID,                                             INVALID,                                                                                                                                                       INVALID,                                             INVALID,
    INVALID,                                             INVALID,                                             INVALID,                                             INVALID,                                             (PWM_2_GENA_ACTCMPAD_ONE | PWM_2_GENA_ACTLOAD_ZERO), (PWM_2_GENB_ACTCMPBD_ONE | PWM_2_GENB_ACTLOAD_ZERO),
    (PWM_2_GENA_ACTCMPAD_ONE | PWM_2_GENA_ACTLOAD_ZERO), (PWM_2_GENB_ACTCMPBD_ONE | PWM_2_GENB_ACTLOAD_ZERO), (PWM_3_GENA_ACTCMPAD_ONE | PWM_3_GENA_ACTLOAD_ZERO), (PWM_3_GENB_ACTCMPBD_ONE | PWM_3_GENB_ACTLOAD_ZERO), INVALID
};

const uint32_t SYSCTL_PRGPIO[NUM_PINS] = {
                                        SYSCTL_PRGPIO_R0, SYSCTL_PRGPIO_R0, SYSCTL_PRGPIO_R0, SYSCTL_PRGPIO_R0, SYSCTL_PRGPIO_R0, SYSCTL_PRGPIO_R0,
    SYSCTL_PRGPIO_R1, SYSCTL_PRGPIO_R1, SYSCTL_PRGPIO_R1, SYSCTL_PRGPIO_R1, SYSCTL_PRGPIO_R1, SYSCTL_PRGPIO_R1, SYSCTL_PRGPIO_R1, SYSCTL_PRGPIO_R1,
                                                                            SYSCTL_PRGPIO_R2, SYSCTL_PRGPIO_R2, SYSCTL_PRGPIO_R2, SYSCTL_PRGPIO_R2,
    SYSCTL_PRGPIO_R3, SYSCTL_PRGPIO_R3, SYSCTL_PRGPIO_R3, SYSCTL_PRGPIO_R3,                                     SYSCTL_PRGPIO_R3, SYSCTL_PRGPIO_R3,
    SYSCTL_PRGPIO_R4, SYSCTL_PRGPIO_R4, SYSCTL_PRGPIO_R4, SYSCTL_PRGPIO_R4, SYSCTL_PRGPIO_R4, SYSCTL_PRGPIO_R4,
    SYSCTL_PRGPIO_R5, SYSCTL_PRGPIO_R5, SYSCTL_PRGPIO_R5, SYSCTL_PRGPIO_R5, SYSCTL_PRGPIO_R5
};

const uint32_t SYSCTL_RCGCGPIO[NUM_PINS] = {
                                            SYSCTL_RCGCGPIO_R0, SYSCTL_RCGCGPIO_R0, SYSCTL_RCGCGPIO_R0, SYSCTL_RCGCGPIO_R0, SYSCTL_RCGCGPIO_R0, SYSCTL_RCGCGPIO_R0,
    SYSCTL_RCGCGPIO_R1, SYSCTL_RCGCGPIO_R1, SYSCTL_RCGCGPIO_R1, SYSCTL_RCGCGPIO_R1, SYSCTL_RCGCGPIO_R1, SYSCTL_RCGCGPIO_R1, SYSCTL_RCGCGPIO_R1, SYSCTL_RCGCGPIO_R1,
                                                                                    SYSCTL_RCGCGPIO_R2, SYSCTL_RCGCGPIO_R2, SYSCTL_RCGCGPIO_R2, SYSCTL_RCGCGPIO_R2,
    SYSCTL_RCGCGPIO_R3, SYSCTL_RCGCGPIO_R3, SYSCTL_RCGCGPIO_R3, SYSCTL_RCGCGPIO_R3,                                         SYSCTL_RCGCGPIO_R3, SYSCTL_RCGCGPIO_R3,
    SYSCTL_RCGCGPIO_R4, SYSCTL_RCGCGPIO_R4, SYSCTL_RCGCGPIO_R4, SYSCTL_RCGCGPIO_R4, SYSCTL_RCGCGPIO_R4, SYSCTL_RCGCGPIO_R4,
    SYSCTL_RCGCGPIO_R5, SYSCTL_RCGCGPIO_R5, SYSCTL_RCGCGPIO_R5, SYSCTL_RCGCGPIO_R5, SYSCTL_RCGCGPIO_R5
};

const uint32_t SYSCTL_RCGCPWM[NUM_PINS] = {
                                          INVALID,           INVALID,           INVALID,           INVALID,           SYSCTL_RCGCPWM_R1, SYSCTL_RCGCPWM_R1,
    INVALID,           INVALID,           INVALID,           INVALID,           SYSCTL_RCGCPWM_R0, SYSCTL_RCGCPWM_R0, SYSCTL_RCGCPWM_R0, SYSCTL_RCGCPWM_R0,
                                                                                SYSCTL_RCGCPWM_R0, SYSCTL_RCGCPWM_R0, INVALID,           INVALID,
    SYSCTL_RCGCPWM_R1, SYSCTL_RCGCPWM_R1, INVALID,           INVALID,                                                 INVALID,           INVALID,
    INVALID,           INVALID,           INVALID,           INVALID,           SYSCTL_RCGCPWM_R0, SYSCTL_RCGCPWM_R0,
    SYSCTL_RCGCPWM_R1, SYSCTL_RCGCPWM_R1, SYSCTL_RCGCPWM_R1, SYSCTL_RCGCPWM_R1, INVALID
};

////////////////////////////////////////////////////////////////////////////////
// End of file
