// main.c
// 
// Car audio control interface for TI TM4C123GXL using Keil v5
// Program takes control values from Tesla Model 3's CAN bus and generates
// corresponding control signals for AudioTech Fischer DSP.
// 
// This main file defines the system's high level behavior.
// 
// This file is part of car_audio_interface v0.3
// Travis Llado, travis@travisllado.com
// Last modified 2020-10-23

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>
#include <stdbool.h>

#include "CAN_4C123/can0.h"
#include "dsp.h"
#include "lladoware_pins.h"
#include "lladoware_system_config.h"
#include "PLL.h"

////////////////////////////////////////////////////////////////////////////////
// Preprocessor Junk

#define GET_BIT(byte, bit_num)  (((byte) & (1 << (bit_num))) >> (bit_num))
#define BITS_PER_BYTE   8

#define WAKEUP_PIN      PC6
#define CHUNK_BIT_NUM   0
#define SCROLL_BIT_NUM  16
#define DISPLAY_BIT_NUM 5

////////////////////////////////////////////////////////////////////////////////
// Internal Prototypes

void update_levels(uint8_t can_data[]);
void update_wakeup(uint8_t can_data[]);
void wakeup_init(void);
void wakeup_off(void);
void wakeup_on(void);

////////////////////////////////////////////////////////////////////////////////
// int main()

int main(void) {
    // Initialize all hardware
    PLL_Init();
    dsp_init();
    wakeup_init();
    CAN0_Open();
    
    // spin forever
    while(1) {
        if(CAN0_GetMailNonBlock(RcvData))
            update_wakeup(RcvData);
        if(CAN1_GetMailNonBlock(RcvData))
            update_levels(RcvData);
    }
};

////////////////////////////////////////////////////////////////////////////////
// update_levels
// modifies DSP control levels based on CAN messages

void update_levels(uint8_t can_data[]){
    const uint8_t VCLEFT_switchStatusIndex = GET_BIT(can_data[CHUNK_BIT_NUM/BITS_PER_BYTE], CHUNK_BIT_NUM%BITS_PER_BYTE);
    const uint8_t VCLEFT_swcLeftScrollTicks = can_data[SCROLL_BIT_NUM/BITS_PER_BYTE];

    if(VCLEFT_switchStatusIndex == 1){
        if(VCLEFT_swcLeftScrollTicks == 0){}
            // do nothing
        else if(VCLEFT_swcLeftScrollTicks < 32)
            dsp_volume_raise_by(VCLEFT_swcLeftScrollTicks);
        else
            dsp_volume_lower_by(64 - VCLEFT_swcLeftScrollTicks);
    }
}

////////////////////////////////////////////////////////////////////////////////
// update_wakeup
// determines how to set switch based on door and occupancy states

void update_wakeup(uint8_t can_data[]){
    const bool UI_displayOn = GET_BIT(can_data[DISPLAY_BIT_NUM/BITS_PER_BYTE], DISPLAY_BIT_NUM%BITS_PER_BYTE);

    if(UI_displayOn)
        wakeup_on();
    else
        wakeup_off();
}

////////////////////////////////////////////////////////////////////////////////
// wakeup_init

void wakeup_init(void){
    init_pin_output(WAKEUP_PIN, 0);
    wakeup_off();
}

////////////////////////////////////////////////////////////////////////////////
// wakeup_off

void wakeup_off(void){
    update_pin(WAKEUP_PIN, LOW);
    onboard_leds_red_update(LOW);
}

////////////////////////////////////////////////////////////////////////////////
// wakeup_on

void wakeup_on(void){
    update_pin(WAKEUP_PIN, HIGH);
    onboard_leds_red_update(HIGH);
}
