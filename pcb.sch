<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="travis_ti">
<packages>
<package name="TM4C123GXL_SHIELD">
<pad name="PE3" x="-20.32" y="3.81" drill="0.9"/>
<pad name="PA6" x="-22.86" y="3.81" drill="0.9"/>
<pad name="PA5" x="-22.86" y="6.35" drill="0.9"/>
<pad name="PE2" x="-20.32" y="6.35" drill="0.9"/>
<pad name="PA7" x="-22.86" y="1.27" drill="0.9"/>
<pad name="PF1" x="-20.32" y="1.27" drill="0.9"/>
<pad name="PB4" x="-22.86" y="8.89" drill="0.9"/>
<pad name="PE1" x="-20.32" y="8.89" drill="0.9"/>
<pad name="PD3" x="-20.32" y="11.43" drill="0.9"/>
<pad name="PD2" x="-20.32" y="13.97" drill="0.9"/>
<pad name="PD1" x="-20.32" y="16.51" drill="0.9"/>
<pad name="PD0" x="-20.32" y="19.05" drill="0.9"/>
<pad name="GND1" x="-20.32" y="21.59" drill="0.9"/>
<pad name="PE5" x="-22.86" y="11.43" drill="0.9"/>
<pad name="PE4" x="-22.86" y="13.97" drill="0.9"/>
<pad name="PB1" x="-22.86" y="16.51" drill="0.9"/>
<pad name="PB0" x="-22.86" y="19.05" drill="0.9"/>
<pad name="PB5" x="-22.86" y="21.59" drill="0.9"/>
<pad name="VBUS1" x="-20.32" y="24.13" drill="0.9"/>
<pad name="3.3V1" x="-22.86" y="24.13" drill="0.9"/>
<pad name="PF4" x="20.32" y="1.27" drill="0.9"/>
<pad name="PD7" x="20.32" y="3.81" drill="0.9"/>
<pad name="PD6" x="20.32" y="6.35" drill="0.9"/>
<pad name="PC7" x="20.32" y="8.89" drill="0.9"/>
<pad name="PC6" x="20.32" y="11.43" drill="0.9"/>
<pad name="PC5" x="20.32" y="13.97" drill="0.9"/>
<pad name="PC4" x="20.32" y="16.51" drill="0.9"/>
<pad name="PB3" x="20.32" y="19.05" drill="0.9"/>
<pad name="PF3" x="20.32" y="21.59" drill="0.9"/>
<pad name="PF2" x="20.32" y="24.13" drill="0.9"/>
<pad name="GND2" x="22.86" y="24.13" drill="0.9"/>
<pad name="PB2" x="22.86" y="21.59" drill="0.9"/>
<pad name="PE0" x="22.86" y="19.05" drill="0.9"/>
<pad name="PF0" x="22.86" y="16.51" drill="0.9"/>
<pad name="RST1" x="22.86" y="13.97" drill="0.9"/>
<pad name="PB7" x="22.86" y="11.43" drill="0.9"/>
<pad name="PB6" x="22.86" y="8.89" drill="0.9"/>
<pad name="PA4" x="22.86" y="6.35" drill="0.9"/>
<pad name="PA3" x="22.86" y="3.81" drill="0.9"/>
<pad name="PA2" x="22.86" y="1.27" drill="0.9"/>
<wire x1="-24.13" y1="0" x2="-24.13" y2="25.4" width="0.127" layer="22"/>
<wire x1="-24.13" y1="25.4" x2="24.13" y2="25.4" width="0.127" layer="22"/>
<wire x1="24.13" y1="25.4" x2="24.13" y2="0" width="0.127" layer="22"/>
<wire x1="24.13" y1="0" x2="-24.13" y2="0" width="0.127" layer="22"/>
</package>
<package name="SOIC-8">
<smd name="P6" x="2.5" y="-0.63" dx="1.5" dy="0.8" layer="1"/>
<smd name="P7" x="2.5" y="0.64" dx="1.5" dy="0.8" layer="1"/>
<smd name="P5" x="2.5" y="-1.9" dx="1.5" dy="0.8" layer="1"/>
<smd name="P8" x="2.5" y="1.91" dx="1.5" dy="0.8" layer="1"/>
<smd name="P1" x="-2.5" y="1.91" dx="1.5" dy="0.8" layer="1"/>
<smd name="P2" x="-2.5" y="0.64" dx="1.5" dy="0.8" layer="1"/>
<smd name="P3" x="-2.5" y="-0.63" dx="1.5" dy="0.8" layer="1"/>
<smd name="P4" x="-2.5" y="-1.9" dx="1.5" dy="0.8" layer="1"/>
<wire x1="-2" y1="-2.5" x2="2" y2="-2.5" width="0.127" layer="21"/>
<wire x1="2" y1="-2.5" x2="2" y2="2.5" width="0.127" layer="21"/>
<wire x1="2" y1="2.5" x2="-2" y2="2.5" width="0.127" layer="21"/>
<wire x1="-2" y1="2.5" x2="-2" y2="-2.5" width="0.127" layer="21"/>
<circle x="-1.5" y="2" radius="0.1" width="0.127" layer="21"/>
<text x="0" y="3" size="0.7" layer="21" font="vector" align="center">&gt;NAME</text>
</package>
<package name="SOT-23">
<smd name="P2" x="-1.3" y="0" dx="1.1" dy="0.6" layer="1"/>
<smd name="P5" x="1.3" y="0" dx="1.1" dy="0.6" layer="1"/>
<smd name="P3" x="-1.3" y="-0.95" dx="1.1" dy="0.6" layer="1"/>
<smd name="P1" x="-1.3" y="0.95" dx="1.1" dy="0.6" layer="1"/>
<smd name="P6" x="1.3" y="0.95" dx="1.1" dy="0.6" layer="1"/>
<smd name="P4" x="1.3" y="-0.95" dx="1.1" dy="0.6" layer="1"/>
<wire x1="-0.9" y1="1.55" x2="0.9" y2="1.55" width="0.127" layer="21"/>
<wire x1="0.9" y1="1.55" x2="0.9" y2="-1.55" width="0.127" layer="21"/>
<wire x1="0.9" y1="-1.55" x2="-0.9" y2="-1.55" width="0.127" layer="21"/>
<wire x1="-0.9" y1="-1.55" x2="-0.9" y2="1.55" width="0.127" layer="21"/>
<rectangle x1="-0.9" y1="-1.55" x2="0.9" y2="1.55" layer="39"/>
<text x="0" y="2" size="0.7" layer="25" font="vector" align="center">&gt;NAME</text>
<circle x="-0.6" y="1.25" radius="0.1" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="TM4C123GXL">
<pin name="PA7" x="-5.08" y="2.54" visible="pin" length="middle"/>
<pin name="PA6" x="-5.08" y="7.62" visible="pin" length="middle"/>
<pin name="PA5" x="-5.08" y="12.7" visible="pin" length="middle"/>
<pin name="PB4" x="-5.08" y="17.78" visible="pin" length="middle"/>
<pin name="PE5" x="-5.08" y="22.86" visible="pin" length="middle"/>
<pin name="PE4" x="-5.08" y="27.94" visible="pin" length="middle"/>
<pin name="PB1" x="-5.08" y="33.02" visible="pin" length="middle"/>
<pin name="PB0" x="-5.08" y="38.1" visible="pin" length="middle"/>
<pin name="PB5" x="-5.08" y="43.18" visible="pin" length="middle"/>
<pin name="3.3V" x="-5.08" y="48.26" visible="pin" length="middle"/>
<pin name="PF1" x="-2.54" y="5.08" visible="pin" length="short"/>
<pin name="PE3" x="-2.54" y="10.16" visible="pin" length="short"/>
<pin name="PE2" x="-2.54" y="15.24" visible="pin" length="short"/>
<pin name="PE1" x="-2.54" y="20.32" visible="pin" length="short"/>
<pin name="PD3" x="-2.54" y="25.4" visible="pin" length="short"/>
<pin name="PD2" x="-2.54" y="30.48" visible="pin" length="short"/>
<pin name="PD1" x="-2.54" y="35.56" visible="pin" length="short"/>
<pin name="GND" x="-2.54" y="45.72" visible="pin" length="short"/>
<pin name="VBUS" x="-2.54" y="50.8" visible="pin" length="short"/>
<pin name="PD0" x="-2.54" y="40.64" visible="pin" length="short"/>
<text x="7.62" y="55.88" size="2.54" layer="95" font="vector" align="center">&gt;NAME</text>
<pin name="PF2" x="17.78" y="48.26" visible="pin" length="short" rot="R180"/>
<pin name="PF3" x="17.78" y="43.18" visible="pin" length="short" rot="R180"/>
<pin name="PB3" x="17.78" y="38.1" visible="pin" length="short" rot="R180"/>
<pin name="PC4" x="17.78" y="33.02" visible="pin" length="short" rot="R180"/>
<pin name="PC5" x="17.78" y="27.94" visible="pin" length="short" rot="R180"/>
<pin name="PC6" x="17.78" y="22.86" visible="pin" length="short" rot="R180"/>
<pin name="PC7" x="17.78" y="17.78" visible="pin" length="short" rot="R180"/>
<pin name="PD6" x="17.78" y="12.7" visible="pin" length="short" rot="R180"/>
<pin name="PD7" x="17.78" y="7.62" visible="pin" length="short" rot="R180"/>
<pin name="PF4" x="17.78" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="PB2" x="20.32" y="45.72" visible="pin" length="middle" rot="R180"/>
<pin name="PE0" x="20.32" y="40.64" visible="pin" length="middle" rot="R180"/>
<pin name="PF0" x="20.32" y="35.56" visible="pin" length="middle" rot="R180"/>
<pin name="RST" x="20.32" y="30.48" visible="pin" length="middle" rot="R180"/>
<pin name="PB7" x="20.32" y="25.4" visible="pin" length="middle" rot="R180"/>
<pin name="PB6" x="20.32" y="20.32" visible="pin" length="middle" rot="R180"/>
<pin name="PA4" x="20.32" y="15.24" visible="pin" length="middle" rot="R180"/>
<pin name="PA3" x="20.32" y="10.16" visible="pin" length="middle" rot="R180"/>
<pin name="PA2" x="20.32" y="5.08" visible="pin" length="middle" rot="R180"/>
<wire x1="0" y1="53.34" x2="15.24" y2="53.34" width="0.254" layer="94"/>
<wire x1="15.24" y1="53.34" x2="15.24" y2="0" width="0.254" layer="94"/>
<wire x1="15.24" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="53.34" width="0.254" layer="94"/>
<wire x1="13.462" y1="47.498" x2="13.97" y2="47.498" width="0.254" layer="95"/>
<wire x1="13.97" y1="47.498" x2="13.97" y2="49.022" width="0.254" layer="95"/>
<wire x1="13.97" y1="49.022" x2="14.478" y2="49.022" width="0.254" layer="95"/>
<wire x1="14.478" y1="49.022" x2="14.478" y2="47.498" width="0.254" layer="95"/>
<wire x1="13.462" y1="42.418" x2="13.97" y2="42.418" width="0.254" layer="95"/>
<wire x1="13.97" y1="42.418" x2="13.97" y2="43.942" width="0.254" layer="95"/>
<wire x1="13.97" y1="43.942" x2="14.478" y2="43.942" width="0.254" layer="95"/>
<wire x1="14.478" y1="43.942" x2="14.478" y2="42.418" width="0.254" layer="95"/>
<wire x1="0.762" y1="42.418" x2="1.27" y2="42.418" width="0.254" layer="95"/>
<wire x1="1.27" y1="42.418" x2="1.27" y2="43.942" width="0.254" layer="95"/>
<wire x1="1.27" y1="43.942" x2="1.778" y2="43.942" width="0.254" layer="95"/>
<wire x1="1.778" y1="43.942" x2="1.778" y2="42.418" width="0.254" layer="95"/>
<wire x1="0.762" y1="39.878" x2="1.27" y2="39.878" width="0.254" layer="95"/>
<wire x1="1.27" y1="39.878" x2="1.27" y2="41.402" width="0.254" layer="95"/>
<wire x1="1.27" y1="41.402" x2="1.778" y2="41.402" width="0.254" layer="95"/>
<wire x1="1.778" y1="41.402" x2="1.778" y2="39.878" width="0.254" layer="95"/>
<wire x1="0.762" y1="34.798" x2="1.27" y2="34.798" width="0.254" layer="95"/>
<wire x1="1.27" y1="34.798" x2="1.27" y2="36.322" width="0.254" layer="95"/>
<wire x1="1.27" y1="36.322" x2="1.778" y2="36.322" width="0.254" layer="95"/>
<wire x1="1.778" y1="36.322" x2="1.778" y2="34.798" width="0.254" layer="95"/>
<wire x1="14.478" y1="34.798" x2="14.478" y2="36.322" width="0.254" layer="95"/>
<wire x1="14.478" y1="36.322" x2="13.97" y2="36.322" width="0.254" layer="95"/>
<wire x1="13.97" y1="36.322" x2="13.97" y2="34.798" width="0.254" layer="95"/>
<wire x1="13.97" y1="34.798" x2="13.462" y2="34.798" width="0.254" layer="95"/>
<wire x1="13.462" y1="32.258" x2="13.97" y2="32.258" width="0.254" layer="95"/>
<wire x1="13.97" y1="32.258" x2="13.97" y2="33.782" width="0.254" layer="95"/>
<wire x1="13.97" y1="33.782" x2="14.478" y2="33.782" width="0.254" layer="95"/>
<wire x1="14.478" y1="33.782" x2="14.478" y2="32.258" width="0.254" layer="95"/>
<wire x1="13.462" y1="27.178" x2="13.97" y2="27.178" width="0.254" layer="95"/>
<wire x1="13.97" y1="27.178" x2="13.97" y2="28.702" width="0.254" layer="95"/>
<wire x1="13.97" y1="28.702" x2="14.478" y2="28.702" width="0.254" layer="95"/>
<wire x1="14.478" y1="28.702" x2="14.478" y2="27.178" width="0.254" layer="95"/>
<wire x1="0.762" y1="27.178" x2="1.27" y2="27.178" width="0.254" layer="95"/>
<wire x1="1.27" y1="27.178" x2="1.27" y2="28.702" width="0.254" layer="95"/>
<wire x1="1.27" y1="28.702" x2="1.778" y2="28.702" width="0.254" layer="95"/>
<wire x1="1.778" y1="28.702" x2="1.778" y2="27.178" width="0.254" layer="95"/>
<wire x1="13.462" y1="24.638" x2="13.97" y2="24.638" width="0.254" layer="95"/>
<wire x1="13.97" y1="24.638" x2="13.97" y2="26.162" width="0.254" layer="95"/>
<wire x1="13.97" y1="26.162" x2="14.478" y2="26.162" width="0.254" layer="95"/>
<wire x1="14.478" y1="26.162" x2="14.478" y2="24.638" width="0.254" layer="95"/>
<wire x1="0.762" y1="22.098" x2="1.27" y2="22.098" width="0.254" layer="95"/>
<wire x1="1.27" y1="22.098" x2="1.27" y2="23.622" width="0.254" layer="95"/>
<wire x1="1.27" y1="23.622" x2="1.778" y2="23.622" width="0.254" layer="95"/>
<wire x1="1.778" y1="23.622" x2="1.778" y2="22.098" width="0.254" layer="95"/>
<wire x1="13.462" y1="19.558" x2="13.97" y2="19.558" width="0.254" layer="95"/>
<wire x1="13.97" y1="19.558" x2="13.97" y2="21.082" width="0.254" layer="95"/>
<wire x1="13.97" y1="21.082" x2="14.478" y2="21.082" width="0.254" layer="95"/>
<wire x1="14.478" y1="21.082" x2="14.478" y2="19.558" width="0.254" layer="95"/>
<wire x1="0.762" y1="17.018" x2="1.27" y2="17.018" width="0.254" layer="95"/>
<wire x1="1.27" y1="17.018" x2="1.27" y2="18.542" width="0.254" layer="95"/>
<wire x1="1.27" y1="18.542" x2="1.778" y2="18.542" width="0.254" layer="95"/>
<wire x1="1.778" y1="18.542" x2="1.778" y2="17.018" width="0.254" layer="95"/>
<wire x1="0.762" y1="6.858" x2="1.27" y2="6.858" width="0.254" layer="95"/>
<wire x1="1.27" y1="6.858" x2="1.27" y2="8.382" width="0.254" layer="95"/>
<wire x1="1.27" y1="8.382" x2="1.778" y2="8.382" width="0.254" layer="95"/>
<wire x1="1.778" y1="8.382" x2="1.778" y2="6.858" width="0.254" layer="95"/>
<wire x1="0.762" y1="4.318" x2="1.27" y2="4.318" width="0.254" layer="95"/>
<wire x1="1.27" y1="4.318" x2="1.27" y2="5.842" width="0.254" layer="95"/>
<wire x1="1.27" y1="5.842" x2="1.778" y2="5.842" width="0.254" layer="95"/>
<wire x1="1.778" y1="5.842" x2="1.778" y2="4.318" width="0.254" layer="95"/>
<wire x1="0.762" y1="1.778" x2="1.27" y2="1.778" width="0.254" layer="95"/>
<wire x1="1.27" y1="1.778" x2="1.27" y2="3.302" width="0.254" layer="95"/>
<wire x1="1.27" y1="3.302" x2="1.778" y2="3.302" width="0.254" layer="95"/>
<wire x1="1.778" y1="3.302" x2="1.778" y2="1.778" width="0.254" layer="95"/>
</symbol>
<symbol name="LM2675">
<pin name="FB" x="17.78" y="7.62" visible="pin" length="short" rot="R180"/>
<pin name="CB" x="17.78" y="5.08" visible="pin" length="short" rot="R180"/>
<pin name="ON" x="-2.54" y="7.62" visible="pin" length="short"/>
<pin name="GND" x="-2.54" y="2.54" visible="pin" length="short"/>
<pin name="VIN" x="-2.54" y="5.08" visible="pin" length="short"/>
<pin name="VSW" x="17.78" y="2.54" visible="pin" length="short" rot="R180"/>
<wire x1="0" y1="0" x2="0" y2="10.16" width="0.254" layer="94"/>
<wire x1="0" y1="10.16" x2="15.24" y2="10.16" width="0.254" layer="94"/>
<wire x1="15.24" y1="10.16" x2="15.24" y2="0" width="0.254" layer="94"/>
<wire x1="15.24" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<text x="7.62" y="12.7" size="1.778" layer="95" font="vector" align="center">&gt;NAME</text>
</symbol>
<symbol name="TCAN1051-Q1">
<pin name="GND" x="-2.54" y="2.54" visible="pin" length="short"/>
<pin name="VIO" x="-2.54" y="5.08" visible="pin" length="short"/>
<pin name="S" x="-2.54" y="7.62" visible="pin" length="short"/>
<pin name="RXD" x="-2.54" y="10.16" visible="pin" length="short"/>
<pin name="TXD" x="-2.54" y="12.7" visible="pin" length="short"/>
<pin name="VCC" x="-2.54" y="15.24" visible="pin" length="short"/>
<pin name="CANL" x="17.78" y="7.62" visible="pin" length="short" rot="R180"/>
<pin name="CANH" x="17.78" y="10.16" visible="pin" length="short" rot="R180"/>
<wire x1="0" y1="17.78" x2="15.24" y2="17.78" width="0.254" layer="94"/>
<wire x1="15.24" y1="17.78" x2="15.24" y2="0" width="0.254" layer="94"/>
<wire x1="15.24" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="17.78" width="0.254" layer="94"/>
<text x="7.62" y="20.32" size="1.778" layer="95" font="vector" align="center">&gt;NAME</text>
</symbol>
<symbol name="LM5114">
<text x="5.08" y="10.16" size="1.778" layer="95" font="vector" align="center">&gt;NAME</text>
<pin name="INB" x="-2.54" y="2.54" visible="off" length="short"/>
<pin name="IN" x="-2.54" y="5.08" visible="off" length="short"/>
<pin name="VSS" x="-2.54" y="0" visible="off" length="short"/>
<pin name="VDD" x="-2.54" y="7.62" visible="off" length="short"/>
<wire x1="0" y1="7.62" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="6.35" y2="3.81" width="0.254" layer="94"/>
<wire x1="6.35" y1="3.81" x2="0" y2="0" width="0.254" layer="94"/>
<pin name="N_OUT" x="7.62" y="2.54" visible="off" length="point" rot="R180"/>
<pin name="P_OUT" x="7.62" y="5.08" visible="off" length="point" rot="R180"/>
<wire x1="7.62" y1="5.08" x2="4.318" y2="5.08" width="0.1524" layer="94"/>
<wire x1="7.62" y1="2.54" x2="4.318" y2="2.54" width="0.1524" layer="94"/>
<text x="1.524" y="0" size="1.27" layer="95" font="vector" align="center-left">VSS</text>
<text x="1.524" y="7.62" size="1.27" layer="95" font="vector" align="center-left">VDD</text>
<text x="1.016" y="5.334" size="1.778" layer="95" font="vector" align="center">+</text>
<text x="1.016" y="2.794" size="1.778" layer="95" font="vector" align="center">-</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="TM4C123GXL_BREAKOUT">
<gates>
<gate name="G$1" symbol="TM4C123GXL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TM4C123GXL_SHIELD">
<connects>
<connect gate="G$1" pin="3.3V" pad="3.3V1"/>
<connect gate="G$1" pin="GND" pad="GND1 GND2"/>
<connect gate="G$1" pin="PA2" pad="PA2"/>
<connect gate="G$1" pin="PA3" pad="PA3"/>
<connect gate="G$1" pin="PA4" pad="PA4"/>
<connect gate="G$1" pin="PA5" pad="PA5"/>
<connect gate="G$1" pin="PA6" pad="PA6"/>
<connect gate="G$1" pin="PA7" pad="PA7"/>
<connect gate="G$1" pin="PB0" pad="PB0"/>
<connect gate="G$1" pin="PB1" pad="PB1"/>
<connect gate="G$1" pin="PB2" pad="PB2"/>
<connect gate="G$1" pin="PB3" pad="PB3"/>
<connect gate="G$1" pin="PB4" pad="PB4"/>
<connect gate="G$1" pin="PB5" pad="PB5"/>
<connect gate="G$1" pin="PB6" pad="PB6"/>
<connect gate="G$1" pin="PB7" pad="PB7"/>
<connect gate="G$1" pin="PC4" pad="PC4"/>
<connect gate="G$1" pin="PC5" pad="PC5"/>
<connect gate="G$1" pin="PC6" pad="PC6"/>
<connect gate="G$1" pin="PC7" pad="PC7"/>
<connect gate="G$1" pin="PD0" pad="PD0"/>
<connect gate="G$1" pin="PD1" pad="PD1"/>
<connect gate="G$1" pin="PD2" pad="PD2"/>
<connect gate="G$1" pin="PD3" pad="PD3"/>
<connect gate="G$1" pin="PD6" pad="PD6"/>
<connect gate="G$1" pin="PD7" pad="PD7"/>
<connect gate="G$1" pin="PE0" pad="PE0"/>
<connect gate="G$1" pin="PE1" pad="PE1"/>
<connect gate="G$1" pin="PE2" pad="PE2"/>
<connect gate="G$1" pin="PE3" pad="PE3"/>
<connect gate="G$1" pin="PE4" pad="PE4"/>
<connect gate="G$1" pin="PE5" pad="PE5"/>
<connect gate="G$1" pin="PF0" pad="PF0"/>
<connect gate="G$1" pin="PF1" pad="PF1"/>
<connect gate="G$1" pin="PF2" pad="PF2"/>
<connect gate="G$1" pin="PF3" pad="PF3"/>
<connect gate="G$1" pin="PF4" pad="PF4"/>
<connect gate="G$1" pin="RST" pad="RST1"/>
<connect gate="G$1" pin="VBUS" pad="VBUS1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LM2675">
<gates>
<gate name="G$1" symbol="LM2675" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC-8">
<connects>
<connect gate="G$1" pin="CB" pad="P1"/>
<connect gate="G$1" pin="FB" pad="P4"/>
<connect gate="G$1" pin="GND" pad="P6"/>
<connect gate="G$1" pin="ON" pad="P5"/>
<connect gate="G$1" pin="VIN" pad="P7"/>
<connect gate="G$1" pin="VSW" pad="P8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TCAN1051-Q1">
<gates>
<gate name="G$1" symbol="TCAN1051-Q1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC-8">
<connects>
<connect gate="G$1" pin="CANH" pad="P7"/>
<connect gate="G$1" pin="CANL" pad="P6"/>
<connect gate="G$1" pin="GND" pad="P2"/>
<connect gate="G$1" pin="RXD" pad="P4"/>
<connect gate="G$1" pin="S" pad="P8"/>
<connect gate="G$1" pin="TXD" pad="P1"/>
<connect gate="G$1" pin="VCC" pad="P3"/>
<connect gate="G$1" pin="VIO" pad="P5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LM5114">
<gates>
<gate name="G$1" symbol="LM5114" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT-23">
<connects>
<connect gate="G$1" pin="IN" pad="P6"/>
<connect gate="G$1" pin="INB" pad="P5"/>
<connect gate="G$1" pin="N_OUT" pad="P3"/>
<connect gate="G$1" pin="P_OUT" pad="P2"/>
<connect gate="G$1" pin="VDD" pad="P1"/>
<connect gate="G$1" pin="VSS" pad="P4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="travis_generic">
<packages>
<package name="0603">
<smd name="P$1" x="0.825" y="0" dx="0.8" dy="1" layer="1"/>
<smd name="P$2" x="-0.825" y="0" dx="0.8" dy="1" layer="1"/>
<wire x1="-0.425" y1="0.45" x2="0.425" y2="0.45" width="0.127" layer="21"/>
<wire x1="0.425" y1="-0.45" x2="-0.425" y2="-0.45" width="0.127" layer="21"/>
<text x="0" y="0.9" size="0.7" layer="21" font="vector" align="center">&gt;NAME</text>
<rectangle x1="-0.425" y1="-0.5" x2="0.425" y2="0.5" layer="39"/>
</package>
<package name="0805">
<smd name="P$1" x="1" y="0" dx="1" dy="1.25" layer="1"/>
<smd name="P$2" x="-1" y="0" dx="1" dy="1.25" layer="1"/>
<wire x1="-1.2" y1="0.7" x2="1.2" y2="0.7" width="0.127" layer="21"/>
<wire x1="1.2" y1="0.7" x2="1.2" y2="-0.7" width="0.127" layer="21"/>
<wire x1="1.2" y1="-0.7" x2="-1.2" y2="-0.7" width="0.127" layer="21"/>
<wire x1="-1.2" y1="-0.7" x2="-1.2" y2="0.7" width="0.127" layer="21"/>
<text x="0" y="1.2" size="0.7" layer="25" font="vector" align="center">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="CAP">
<pin name="P$1" x="-5.08" y="0" visible="off" length="short"/>
<pin name="P$2" x="2.54" y="0" visible="off" length="short" rot="R180"/>
<wire x1="-1.778" y1="0" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<text x="-1.778" y="3.302" size="1.778" layer="95" font="vector" align="center">&gt;NAME</text>
<wire x1="-1.778" y1="1.778" x2="-1.778" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-1.778" x2="-0.762" y2="1.778" width="0.254" layer="94"/>
</symbol>
<symbol name="RES">
<pin name="P$1" x="-5.08" y="0" visible="off" length="short"/>
<pin name="P$2" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<text x="0" y="2.54" size="1.778" layer="95" font="vector" align="center">&gt;NAME</text>
<wire x1="-2.54" y1="0" x2="-2.286" y2="1.016" width="0.254" layer="94"/>
<wire x1="-2.286" y1="1.016" x2="-1.778" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-1.016" x2="-1.27" y2="1.016" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.016" x2="-0.762" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-1.016" x2="-0.254" y2="1.016" width="0.254" layer="94"/>
<wire x1="-0.254" y1="1.016" x2="0.254" y2="-1.016" width="0.254" layer="94"/>
<wire x1="0.254" y1="-1.016" x2="0.762" y2="1.016" width="0.254" layer="94"/>
<wire x1="0.762" y1="1.016" x2="1.27" y2="-1.016" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.016" x2="1.778" y2="1.016" width="0.254" layer="94"/>
<wire x1="1.778" y1="1.016" x2="2.286" y2="-1.016" width="0.254" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAP_0603">
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0603">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RES_0603">
<gates>
<gate name="G$1" symbol="RES" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0603">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP_0805">
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0805">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="travis_panasonic">
<packages>
<package name="ETQ-P4M___YFN">
<rectangle x1="-3.55" y1="-3.5" x2="3.55" y2="3.5" layer="39"/>
<smd name="P$1" x="2.9" y="0" dx="3" dy="3.6" layer="1"/>
<smd name="P$2" x="-2.9" y="0" dx="3" dy="3.6" layer="1"/>
<wire x1="-3.55" y1="3.5" x2="3.55" y2="3.5" width="0.127" layer="21"/>
<wire x1="3.55" y1="3.5" x2="3.55" y2="-3.5" width="0.127" layer="21"/>
<wire x1="3.55" y1="-3.5" x2="-3.55" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-3.55" y1="-3.5" x2="-3.55" y2="3.5" width="0.127" layer="21"/>
<text x="0" y="4" size="0.7" layer="21" font="vector" align="center">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="INDUCTOR">
<pin name="P$1" x="-5.08" y="0" visible="off" length="point"/>
<pin name="P$2" x="5.08" y="0" visible="off" length="point"/>
<wire x1="-5.08" y1="0" x2="-3.683" y2="0" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0" x2="3.683" y2="0" width="0.1524" layer="94"/>
<wire x1="-3.683" y1="0" x2="-1.397" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="-1.397" y1="0" x2="-2.413" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="-2.413" y1="0" x2="-0.127" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="-0.127" y1="0" x2="-1.143" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="-1.143" y1="0" x2="1.143" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="1.143" y1="0" x2="0.127" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="0.127" y1="0" x2="2.413" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="2.413" y1="0" x2="1.397" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="1.397" y1="0" x2="3.683" y2="0" width="0.254" layer="94" curve="-180"/>
<text x="0" y="2.54" size="1.778" layer="95" font="vector" align="center">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="ETQ-P4M470YFN">
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ETQ-P4M___YFN">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="travis_mcc">
<packages>
<package name="SOT-23-3">
<smd name="P3" x="1.3" y="0" dx="1.1" dy="0.6" layer="1"/>
<smd name="P2" x="-1.3" y="-0.95" dx="1.1" dy="0.6" layer="1"/>
<smd name="P1" x="-1.3" y="0.95" dx="1.1" dy="0.6" layer="1"/>
<wire x1="-0.9" y1="1.55" x2="0.9" y2="1.55" width="0.127" layer="21"/>
<wire x1="0.9" y1="1.55" x2="0.9" y2="-1.55" width="0.127" layer="21"/>
<wire x1="0.9" y1="-1.55" x2="-0.9" y2="-1.55" width="0.127" layer="21"/>
<wire x1="-0.9" y1="-1.55" x2="-0.9" y2="1.55" width="0.127" layer="21"/>
<rectangle x1="-0.9" y1="-1.55" x2="0.9" y2="1.55" layer="39"/>
<text x="0" y="2" size="0.7" layer="25" font="vector" align="center">&gt;NAME</text>
</package>
<package name="SO-8">
<smd name="P6" x="2.5" y="-0.63" dx="1.5" dy="0.8" layer="1"/>
<smd name="P7" x="2.5" y="0.64" dx="1.5" dy="0.8" layer="1"/>
<smd name="P5" x="2.5" y="-1.9" dx="1.5" dy="0.8" layer="1"/>
<smd name="P8" x="2.5" y="1.91" dx="1.5" dy="0.8" layer="1"/>
<smd name="P1" x="-2.5" y="1.91" dx="1.5" dy="0.8" layer="1"/>
<smd name="P2" x="-2.5" y="0.64" dx="1.5" dy="0.8" layer="1"/>
<smd name="P3" x="-2.5" y="-0.63" dx="1.5" dy="0.8" layer="1"/>
<smd name="P4" x="-2.5" y="-1.9" dx="1.5" dy="0.8" layer="1"/>
<wire x1="-2" y1="-2.5" x2="2" y2="-2.5" width="0.127" layer="21"/>
<wire x1="2" y1="-2.5" x2="2" y2="2.5" width="0.127" layer="21"/>
<wire x1="2" y1="2.5" x2="-2" y2="2.5" width="0.127" layer="21"/>
<wire x1="-2" y1="2.5" x2="-2" y2="-2.5" width="0.127" layer="21"/>
<circle x="-1.5" y="2" radius="0.1" width="0.127" layer="21"/>
<text x="0" y="3" size="0.7" layer="21" font="vector" align="center">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="SCHOTTKY_DIODE">
<pin name="ANODE" x="-2.54" y="0" visible="off" length="short"/>
<pin name="CATHODE" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<text x="1.27" y="2.794" size="1.778" layer="95" font="vector" align="center">&gt;NAME</text>
<wire x1="0" y1="-1.524" x2="0" y2="1.524" width="0.254" layer="94"/>
<wire x1="0" y1="1.524" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="0" y2="-1.524" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="1.524" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.524" x2="3.048" y2="1.524" width="0.254" layer="94"/>
<wire x1="3.048" y1="1.524" x2="3.048" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.524" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.524" x2="2.032" y2="-1.524" width="0.254" layer="94"/>
<wire x1="2.032" y1="-1.27" x2="2.032" y2="-1.524" width="0.254" layer="94"/>
</symbol>
<symbol name="MOSFET_P">
<pin name="G" x="-5.08" y="0" visible="off" length="short"/>
<pin name="S" x="2.54" y="5.08" visible="off" length="short" rot="R270"/>
<pin name="D" x="2.54" y="-5.08" visible="off" length="short" rot="R90"/>
<wire x1="-1.27" y1="3.81" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-3.81" width="0.254" layer="94"/>
<wire x1="0" y1="-3.81" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.778" width="0.254" layer="94"/>
<wire x1="0" y1="1.778" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="0" y1="1.016" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.762" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="6.35" y="0" size="1.778" layer="95" font="vector" rot="R90" align="center">&gt;NAME</text>
<wire x1="3.81" y1="0.508" x2="3.302" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="3.302" y1="-0.508" x2="3.81" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-0.508" x2="4.318" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="4.318" y1="-0.508" x2="3.81" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.302" y1="0.508" x2="3.81" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.508" x2="4.318" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.508" x2="3.81" y2="3.048" width="0.1524" layer="94"/>
<wire x1="3.81" y1="3.048" x2="2.54" y2="3.048" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-0.508" x2="3.81" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-3.048" x2="2.54" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="4.191" y1="-0.381" x2="3.429" y2="-0.381" width="0.1524" layer="94"/>
<wire x1="3.429" y1="-0.381" x2="3.556" y2="-0.254" width="0.1524" layer="94"/>
<wire x1="3.556" y1="-0.254" x2="4.064" y2="-0.254" width="0.1524" layer="94"/>
<wire x1="4.064" y1="-0.254" x2="4.191" y2="-0.381" width="0.1524" layer="94"/>
<wire x1="4.064" y1="-0.127" x2="3.556" y2="-0.127" width="0.1524" layer="94"/>
<wire x1="3.556" y1="-0.127" x2="3.683" y2="0" width="0.1524" layer="94"/>
<wire x1="3.683" y1="0" x2="3.937" y2="0" width="0.1524" layer="94"/>
<wire x1="3.937" y1="0" x2="4.064" y2="-0.127" width="0.1524" layer="94"/>
<wire x1="3.937" y1="0.127" x2="3.683" y2="0.127" width="0.1524" layer="94"/>
<wire x1="3.683" y1="0.127" x2="3.81" y2="0.254" width="0.1524" layer="94"/>
<wire x1="3.937" y1="0.127" x2="3.81" y2="0.254" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.254" x2="3.81" y2="0.381" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.032" y2="0.3048" width="0.1524" layer="94"/>
<wire x1="2.032" y1="0.3048" x2="1.27" y2="0.762" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-0.762" x2="2.032" y2="-0.3048" width="0.1524" layer="94"/>
<wire x1="2.032" y1="-0.3048" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="94"/>
<wire x1="1.5748" y1="-0.5588" x2="1.5748" y2="0.5588" width="0.1524" layer="94"/>
<wire x1="1.7272" y1="0.4572" x2="1.7272" y2="-0.4572" width="0.1524" layer="94"/>
<wire x1="1.8796" y1="-0.3556" x2="1.8796" y2="0.3556" width="0.1524" layer="94"/>
<wire x1="2.032" y1="0.3048" x2="2.032" y2="-0.3048" width="0.1524" layer="94"/>
<wire x1="2.1844" y1="-0.2032" x2="2.1844" y2="0.2032" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BAT750">
<gates>
<gate name="G$1" symbol="SCHOTTKY_DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT-23-3">
<connects>
<connect gate="G$1" pin="ANODE" pad="P1"/>
<connect gate="G$1" pin="CATHODE" pad="P3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MCQ4407">
<gates>
<gate name="G$1" symbol="MOSFET_P" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SO-8">
<connects>
<connect gate="G$1" pin="D" pad="P5 P6 P7 P8"/>
<connect gate="G$1" pin="G" pad="P4"/>
<connect gate="G$1" pin="S" pad="P1 P2 P3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="travis_avx">
<packages>
<package name="1210P">
<smd name="CATHODE" x="1.3" y="0" dx="1.4" dy="2.4" layer="1"/>
<smd name="ANODE" x="-1.3" y="0" dx="1.4" dy="2.4" layer="1"/>
<wire x1="1.6" y1="1.5" x2="1.6" y2="-1.5" width="0.127" layer="21"/>
<wire x1="1.6" y1="-1.5" x2="-1.6" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1.6" y1="-1.5" x2="-1.6" y2="1.5" width="0.127" layer="21"/>
<wire x1="-1.6" y1="1.5" x2="1.6" y2="1.5" width="0.127" layer="21"/>
<text x="0" y="2.1" size="0.7" layer="21" font="vector" align="center">&gt;NAME</text>
<rectangle x1="-1.6" y1="-1.5" x2="-0.4" y2="1.5" layer="21"/>
</package>
<package name="2917P">
<smd name="CATHODE" x="3.3" y="0" dx="2.4" dy="3" layer="1"/>
<smd name="ANODE" x="-3.3" y="0" dx="2.4" dy="3" layer="1"/>
<wire x1="-3.7" y1="2.2" x2="3.7" y2="2.2" width="0.127" layer="21"/>
<wire x1="3.7" y1="2.2" x2="3.7" y2="-2.2" width="0.127" layer="21"/>
<wire x1="3.7" y1="-2.2" x2="-3.7" y2="-2.2" width="0.127" layer="21"/>
<wire x1="-3.7" y1="-2.2" x2="-3.7" y2="2.2" width="0.127" layer="21"/>
<rectangle x1="-3.7" y1="-2.2" x2="-1.7" y2="2.2" layer="21"/>
<text x="0" y="2.8" size="0.7" layer="21" font="vector" align="center">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="CAP_P">
<pin name="ANODE" x="-2.54" y="0" visible="off" length="short"/>
<pin name="CATHODE" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<wire x1="1.016" y1="2.032" x2="1.016" y2="0" width="0.254" layer="94"/>
<wire x1="1.016" y1="0" x2="1.016" y2="-2.032" width="0.254" layer="94"/>
<wire x1="2.032" y1="-2.032" x2="2.032" y2="2.032" width="0.254" layer="94" curve="-50.402247"/>
<wire x1="0" y1="0" x2="1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="1.524" y2="0" width="0.1524" layer="94"/>
<text x="1.27" y="3.556" size="1.778" layer="95" font="vector" align="center">&gt;NAME</text>
<wire x1="-0.508" y1="1.016" x2="0.508" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0" y1="0.508" x2="0" y2="1.524" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAP_1210P">
<gates>
<gate name="G$1" symbol="CAP_P" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1210P">
<connects>
<connect gate="G$1" pin="ANODE" pad="ANODE"/>
<connect gate="G$1" pin="CATHODE" pad="CATHODE"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP_2917P">
<gates>
<gate name="G$1" symbol="CAP_P" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2917P">
<connects>
<connect gate="G$1" pin="ANODE" pad="ANODE"/>
<connect gate="G$1" pin="CATHODE" pad="CATHODE"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="travis_molex">
<packages>
<package name="53261-0471">
<smd name="P1" x="-1.875" y="5" dx="0.8" dy="1.6" layer="1"/>
<smd name="P2" x="-0.625" y="5" dx="0.8" dy="1.6" layer="1"/>
<smd name="P3" x="0.625" y="5" dx="0.8" dy="1.6" layer="1"/>
<smd name="P4" x="1.875" y="5" dx="0.8" dy="1.6" layer="1"/>
<smd name="S1" x="-4.425" y="2.1" dx="2.1" dy="3" layer="1"/>
<smd name="S2" x="4.425" y="2.1" dx="2.1" dy="3" layer="1"/>
<wire x1="-4.9" y1="4.2" x2="4.9" y2="4.2" width="0.127" layer="21"/>
<wire x1="4.9" y1="4.2" x2="4.9" y2="0" width="0.127" layer="21"/>
<wire x1="4.9" y1="0" x2="-4.9" y2="0" width="0.127" layer="21"/>
<wire x1="-4.9" y1="0" x2="-4.9" y2="4.2" width="0.127" layer="21"/>
<text x="0" y="6.5" size="0.7" layer="25" font="vector" align="center">&gt;NAME</text>
<rectangle x1="-4.9" y1="0" x2="4.9" y2="4.2" layer="39"/>
</package>
<package name="53261-0671">
<smd name="P2" x="-1.875" y="5" dx="0.8" dy="1.6" layer="1"/>
<smd name="P3" x="-0.625" y="5" dx="0.8" dy="1.6" layer="1"/>
<smd name="P4" x="0.625" y="5" dx="0.8" dy="1.6" layer="1"/>
<smd name="P5" x="1.875" y="5" dx="0.8" dy="1.6" layer="1"/>
<smd name="S1" x="-5.675" y="2.1" dx="2.1" dy="3" layer="1"/>
<smd name="S2" x="5.675" y="2.1" dx="2.1" dy="3" layer="1"/>
<wire x1="-6.15" y1="4.2" x2="6.15" y2="4.2" width="0.127" layer="21"/>
<wire x1="6.15" y1="4.2" x2="6.15" y2="0" width="0.127" layer="21"/>
<wire x1="6.15" y1="0" x2="-6.15" y2="0" width="0.127" layer="21"/>
<wire x1="-6.15" y1="0" x2="-6.15" y2="4.2" width="0.127" layer="21"/>
<smd name="P6" x="3.125" y="5" dx="0.8" dy="1.6" layer="1"/>
<smd name="P1" x="-3.125" y="5" dx="0.8" dy="1.6" layer="1"/>
<text x="0" y="6.5" size="0.7" layer="25" font="vector" align="center">&gt;NAME</text>
<rectangle x1="-6.1" y1="0" x2="6.1" y2="4.2" layer="39"/>
</package>
<package name="53261-0271">
<smd name="P1" x="-0.625" y="5" dx="0.8" dy="1.6" layer="1"/>
<smd name="P2" x="0.625" y="5" dx="0.8" dy="1.6" layer="1"/>
<smd name="S1" x="-3.175" y="2.1" dx="2.1" dy="3" layer="1"/>
<smd name="S2" x="3.175" y="2.1" dx="2.1" dy="3" layer="1"/>
<wire x1="-3.65" y1="4.2" x2="3.65" y2="4.2" width="0.127" layer="21"/>
<wire x1="3.65" y1="4.2" x2="3.65" y2="0" width="0.127" layer="21"/>
<wire x1="3.65" y1="0" x2="-3.65" y2="0" width="0.127" layer="21"/>
<wire x1="-3.65" y1="0" x2="-3.65" y2="4.2" width="0.127" layer="21"/>
<text x="0" y="6.5" size="0.7" layer="25" font="vector" align="center">&gt;NAME</text>
<rectangle x1="-3.6" y1="0" x2="3.6" y2="4.2" layer="39"/>
</package>
<package name="53261-0571">
<smd name="P1" x="-2.5" y="5" dx="0.8" dy="1.6" layer="1"/>
<smd name="P2" x="-1.25" y="5" dx="0.8" dy="1.6" layer="1"/>
<smd name="P3" x="0" y="5" dx="0.8" dy="1.6" layer="1"/>
<smd name="P4" x="1.25" y="5" dx="0.8" dy="1.6" layer="1"/>
<smd name="S1" x="-5.05" y="2.1" dx="2.1" dy="3" layer="1"/>
<smd name="S2" x="5.05" y="2.1" dx="2.1" dy="3" layer="1"/>
<wire x1="-5.525" y1="4.2" x2="5.525" y2="4.2" width="0.127" layer="21"/>
<wire x1="5.525" y1="4.2" x2="5.525" y2="0" width="0.127" layer="21"/>
<wire x1="5.525" y1="0" x2="-5.525" y2="0" width="0.127" layer="21"/>
<wire x1="-5.525" y1="0" x2="-5.525" y2="4.2" width="0.127" layer="21"/>
<text x="0" y="6.5" size="0.7" layer="25" font="vector" align="center">&gt;NAME</text>
<smd name="P5" x="2.5" y="5" dx="0.8" dy="1.6" layer="1"/>
<rectangle x1="-5.5" y1="0" x2="5.5" y2="4.2" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="CON_4X1">
<pin name="P1" x="-2.54" y="2.54" visible="pin" length="short"/>
<pin name="P2" x="-2.54" y="5.08" visible="pin" length="short"/>
<pin name="P3" x="-2.54" y="7.62" visible="pin" length="short"/>
<pin name="P4" x="-2.54" y="10.16" visible="pin" length="short"/>
<wire x1="0" y1="0" x2="10.16" y2="0" width="0.254" layer="94"/>
<wire x1="10.16" y1="0" x2="10.16" y2="12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="12.7" x2="0" y2="12.7" width="0.254" layer="94"/>
<wire x1="0" y1="12.7" x2="0" y2="0" width="0.254" layer="94"/>
<text x="5.08" y="15.24" size="1.778" layer="95" font="vector" align="center">&gt;NAME</text>
</symbol>
<symbol name="CON_6X1">
<pin name="P1" x="-2.54" y="2.54" visible="pin" length="short"/>
<pin name="P2" x="-2.54" y="5.08" visible="pin" length="short"/>
<pin name="P3" x="-2.54" y="7.62" visible="pin" length="short"/>
<pin name="P4" x="-2.54" y="10.16" visible="pin" length="short"/>
<pin name="P5" x="-2.54" y="12.7" visible="pin" length="short"/>
<pin name="P6" x="-2.54" y="15.24" visible="pin" length="short"/>
<wire x1="0" y1="0" x2="10.16" y2="0" width="0.254" layer="94"/>
<wire x1="10.16" y1="0" x2="10.16" y2="17.78" width="0.254" layer="94"/>
<wire x1="10.16" y1="17.78" x2="0" y2="17.78" width="0.254" layer="94"/>
<wire x1="0" y1="17.78" x2="0" y2="0" width="0.254" layer="94"/>
<text x="5.08" y="20.32" size="1.778" layer="95" font="vector" align="center">&gt;NAME</text>
</symbol>
<symbol name="CON_2X1">
<pin name="P$1" x="-2.54" y="2.54" visible="pin" length="short"/>
<pin name="P$2" x="-2.54" y="5.08" visible="pin" length="short"/>
<wire x1="0" y1="0" x2="10.16" y2="0" width="0.254" layer="94"/>
<wire x1="10.16" y1="0" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="0" y2="0" width="0.254" layer="94"/>
<text x="12.7" y="2.54" size="1.778" layer="95" font="vector" rot="R90" align="center">&gt;NAME</text>
</symbol>
<symbol name="CON_5X1">
<pin name="P1" x="-2.54" y="2.54" visible="pin" length="short"/>
<pin name="P2" x="-2.54" y="5.08" visible="pin" length="short"/>
<pin name="P3" x="-2.54" y="7.62" visible="pin" length="short"/>
<pin name="P4" x="-2.54" y="10.16" visible="pin" length="short"/>
<wire x1="0" y1="0" x2="10.16" y2="0" width="0.254" layer="94"/>
<wire x1="10.16" y1="0" x2="10.16" y2="15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="15.24" x2="0" y2="15.24" width="0.254" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="0" width="0.254" layer="94"/>
<text x="5.08" y="17.78" size="1.778" layer="95" font="vector" align="center">&gt;NAME</text>
<pin name="P5" x="-2.54" y="12.7" visible="pin" length="short"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="53261-0471">
<gates>
<gate name="G$1" symbol="CON_4X1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="53261-0471">
<connects>
<connect gate="G$1" pin="P1" pad="P1"/>
<connect gate="G$1" pin="P2" pad="P2"/>
<connect gate="G$1" pin="P3" pad="P3"/>
<connect gate="G$1" pin="P4" pad="P4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="53261-0671">
<gates>
<gate name="G$1" symbol="CON_6X1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="53261-0671">
<connects>
<connect gate="G$1" pin="P1" pad="P1"/>
<connect gate="G$1" pin="P2" pad="P2"/>
<connect gate="G$1" pin="P3" pad="P3"/>
<connect gate="G$1" pin="P4" pad="P4"/>
<connect gate="G$1" pin="P5" pad="P5"/>
<connect gate="G$1" pin="P6" pad="P6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="53261-0271">
<gates>
<gate name="G$1" symbol="CON_2X1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="53261-0271">
<connects>
<connect gate="G$1" pin="P$1" pad="P1"/>
<connect gate="G$1" pin="P$2" pad="P2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="53261-0571">
<gates>
<gate name="G$1" symbol="CON_5X1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="53261-0571">
<connects>
<connect gate="G$1" pin="P1" pad="P1"/>
<connect gate="G$1" pin="P2" pad="P2"/>
<connect gate="G$1" pin="P3" pad="P3"/>
<connect gate="G$1" pin="P4" pad="P4"/>
<connect gate="G$1" pin="P5" pad="P5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="C2" library="travis_generic" deviceset="CAP_0603" device="" value="10n"/>
<part name="C4" library="travis_generic" deviceset="CAP_0603" device="" value="10n"/>
<part name="C5" library="travis_generic" deviceset="CAP_0603" device="" value="1u"/>
<part name="C6" library="travis_generic" deviceset="CAP_0603" device="" value="1u"/>
<part name="R2" library="travis_generic" deviceset="RES_0603" device="" value="1k"/>
<part name="R3" library="travis_generic" deviceset="RES_0603" device="" value="1k"/>
<part name="CON_MCU" library="travis_ti" deviceset="TM4C123GXL_BREAKOUT" device=""/>
<part name="L1" library="travis_panasonic" deviceset="ETQ-P4M470YFN" device="" value="47u"/>
<part name="D1" library="travis_mcc" deviceset="BAT750" device=""/>
<part name="IC1" library="travis_ti" deviceset="LM2675" device=""/>
<part name="IC2" library="travis_ti" deviceset="TCAN1051-Q1" device=""/>
<part name="C3" library="travis_avx" deviceset="CAP_1210P" device="" value="68u"/>
<part name="C1" library="travis_avx" deviceset="CAP_2917P" device="" value="100u"/>
<part name="CON_ENC" library="travis_molex" deviceset="53261-0471" device=""/>
<part name="IC4" library="travis_ti" deviceset="LM5114" device=""/>
<part name="R1" library="travis_generic" deviceset="RES_0603" device="" value="100Ω"/>
<part name="IC5" library="travis_mcc" deviceset="MCQ4407" device=""/>
<part name="C11" library="travis_generic" deviceset="CAP_0805" device="" value="10uF, 25V"/>
<part name="C7" library="travis_generic" deviceset="CAP_0603" device="" value="1u, 25V"/>
<part name="CON_DSP" library="travis_molex" deviceset="53261-0671" device=""/>
<part name="CON_WAKE" library="travis_molex" deviceset="53261-0271" device=""/>
<part name="CON_CAN" library="travis_molex" deviceset="53261-0571" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="22.86" y1="99.06" x2="22.86" y2="76.2" width="0.1524" layer="97" style="shortdash"/>
<wire x1="22.86" y1="76.2" x2="78.74" y2="76.2" width="0.1524" layer="97" style="shortdash"/>
<wire x1="78.74" y1="76.2" x2="78.74" y2="99.06" width="0.1524" layer="97" style="shortdash"/>
<wire x1="78.74" y1="99.06" x2="22.86" y2="99.06" width="0.1524" layer="97" style="shortdash"/>
<wire x1="60.96" y1="45.72" x2="22.86" y2="45.72" width="0.1524" layer="97" style="shortdash"/>
<wire x1="22.86" y1="45.72" x2="22.86" y2="71.12" width="0.1524" layer="97" style="shortdash"/>
<wire x1="22.86" y1="71.12" x2="60.96" y2="71.12" width="0.1524" layer="97" style="shortdash"/>
<wire x1="60.96" y1="43.18" x2="40.64" y2="43.18" width="0.1524" layer="97" style="shortdash"/>
<wire x1="40.64" y1="43.18" x2="40.64" y2="10.16" width="0.1524" layer="97" style="shortdash"/>
<wire x1="40.64" y1="10.16" x2="60.96" y2="10.16" width="0.1524" layer="97" style="shortdash"/>
<wire x1="60.96" y1="10.16" x2="60.96" y2="43.18" width="0.1524" layer="97" style="shortdash"/>
<text x="60.96" y="96.52" size="1.778" layer="97" font="vector" align="center">VOLTAGE REGULATOR</text>
<text x="38.1" y="48.26" size="1.778" layer="95" font="vector" align="center">CAN TRANSCEIVER</text>
<text x="88.9" y="76.2" size="1.778" layer="95" font="vector" align="center">CON_MCU</text>
<wire x1="60.96" y1="71.12" x2="60.96" y2="45.72" width="0.1524" layer="97" style="shortdash"/>
<wire x1="109.22" y1="25.4" x2="160.02" y2="25.4" width="0.1524" layer="97" style="shortdash"/>
<wire x1="160.02" y1="25.4" x2="160.02" y2="5.08" width="0.1524" layer="97" style="shortdash"/>
<wire x1="160.02" y1="5.08" x2="109.22" y2="5.08" width="0.1524" layer="97" style="shortdash"/>
<wire x1="109.22" y1="5.08" x2="109.22" y2="25.4" width="0.1524" layer="97" style="shortdash"/>
<text x="134.62" y="7.62" size="1.778" layer="97" font="vector" align="center">WAKEUP AMP</text>
<text x="50.8" y="40.64" size="1.778" layer="97" font="vector" rot="MR0" align="center">RC FILTERS</text>
</plain>
<instances>
<instance part="C2" gate="G$1" x="58.42" y="88.9" smashed="yes">
<attribute name="NAME" x="59.944" y="90.932" size="1.778" layer="95" font="vector" align="center"/>
</instance>
<instance part="C4" gate="G$1" x="55.88" y="63.5" smashed="yes" rot="R90">
<attribute name="NAME" x="57.658" y="59.69" size="1.778" layer="95" font="vector" rot="R90" align="center"/>
</instance>
<instance part="C5" gate="G$1" x="50.8" y="17.78" smashed="yes" rot="MR90">
<attribute name="NAME" x="54.102" y="16.51" size="1.778" layer="95" font="vector" rot="MR90" align="center"/>
</instance>
<instance part="C6" gate="G$1" x="45.72" y="17.78" smashed="yes" rot="MR90">
<attribute name="NAME" x="42.418" y="16.51" size="1.778" layer="95" font="vector" rot="MR90" align="center"/>
</instance>
<instance part="R2" gate="G$1" x="50.8" y="30.48" smashed="yes" rot="MR90">
<attribute name="NAME" x="53.34" y="30.48" size="1.778" layer="95" font="vector" rot="MR90" align="center"/>
</instance>
<instance part="R3" gate="G$1" x="45.72" y="30.48" smashed="yes" rot="MR90">
<attribute name="NAME" x="48.26" y="30.48" size="1.778" layer="95" font="vector" rot="MR90" align="center"/>
</instance>
<instance part="CON_MCU" gate="G$1" x="81.28" y="20.32" smashed="yes"/>
<instance part="L1" gate="G$1" x="66.04" y="86.36" smashed="yes">
<attribute name="NAME" x="66.04" y="89.408" size="1.778" layer="95" font="vector" align="center"/>
</instance>
<instance part="D1" gate="G$1" x="53.34" y="81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="56.642" y="82.55" size="1.778" layer="95" font="vector" rot="R90" align="center"/>
</instance>
<instance part="IC1" gate="G$1" x="35.56" y="83.82" smashed="yes">
<attribute name="NAME" x="43.18" y="81.28" size="1.778" layer="95" font="vector" align="center"/>
</instance>
<instance part="IC2" gate="G$1" x="43.18" y="50.8" smashed="yes" rot="MR0">
<attribute name="NAME" x="45.72" y="68.58" size="1.778" layer="95" font="vector" rot="MR0" align="center"/>
</instance>
<instance part="C3" gate="G$1" x="71.12" y="83.82" smashed="yes" rot="R270">
<attribute name="NAME" x="67.564" y="82.296" size="1.778" layer="95" font="vector" rot="R270" align="center"/>
</instance>
<instance part="C1" gate="G$1" x="30.48" y="86.36" smashed="yes" rot="R270">
<attribute name="NAME" x="28.448" y="82.296" size="1.778" layer="95" font="vector" rot="R270" align="center"/>
</instance>
<instance part="CON_ENC" gate="G$1" x="114.3" y="40.64" smashed="yes" rot="MR180">
<attribute name="NAME" x="119.38" y="43.18" size="1.778" layer="95" font="vector" rot="MR180" align="center"/>
</instance>
<instance part="IC4" gate="G$1" x="124.46" y="12.7" smashed="yes">
<attribute name="NAME" x="132.08" y="12.7" size="1.778" layer="95" font="vector" align="center"/>
</instance>
<instance part="R1" gate="G$1" x="137.16" y="17.78" smashed="yes">
<attribute name="NAME" x="137.16" y="20.32" size="1.778" layer="95" font="vector" align="center"/>
</instance>
<instance part="IC5" gate="G$1" x="147.32" y="17.78" smashed="yes">
<attribute name="NAME" x="143.51" y="12.7" size="1.778" layer="95" font="vector" rot="R180" align="center"/>
</instance>
<instance part="C11" gate="G$1" x="154.94" y="20.32" smashed="yes" rot="R90">
<attribute name="NAME" x="156.718" y="16.002" size="1.778" layer="95" font="vector" rot="R90" align="center"/>
</instance>
<instance part="C7" gate="G$1" x="114.3" y="17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="110.998" y="17.78" size="1.778" layer="95" font="vector" rot="R90" align="center"/>
</instance>
<instance part="CON_DSP" gate="G$1" x="33.02" y="10.16" smashed="yes" rot="MR0">
<attribute name="NAME" x="27.94" y="30.48" size="1.778" layer="95" font="vector" rot="MR0" align="center"/>
</instance>
<instance part="CON_WAKE" gate="G$1" x="165.1" y="7.62" smashed="yes">
<attribute name="NAME" x="170.18" y="17.78" size="1.778" layer="95" font="vector" rot="R180" align="center"/>
</instance>
<instance part="CON_CAN" gate="G$1" x="12.7" y="66.04" smashed="yes" rot="MR0">
<attribute name="NAME" x="7.62" y="83.82" size="1.778" layer="95" font="vector" rot="MR0" align="center"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="V_VOL" class="0">
<segment>
<pinref part="C5" gate="G$1" pin="P$2"/>
<pinref part="R2" gate="G$1" pin="P$1"/>
<wire x1="50.8" y1="25.4" x2="50.8" y2="22.86" width="0.1524" layer="91"/>
<wire x1="50.8" y1="22.86" x2="50.8" y2="20.32" width="0.1524" layer="91"/>
<junction x="50.8" y="22.86"/>
<wire x1="50.8" y1="22.86" x2="43.18" y2="22.86" width="0.1524" layer="91"/>
<pinref part="CON_DSP" gate="G$1" pin="P4"/>
<wire x1="35.56" y1="20.32" x2="43.18" y2="20.32" width="0.1524" layer="91"/>
<wire x1="43.18" y1="20.32" x2="43.18" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="V_BASS" class="0">
<segment>
<pinref part="C6" gate="G$1" pin="P$2"/>
<pinref part="R3" gate="G$1" pin="P$1"/>
<wire x1="45.72" y1="25.4" x2="45.72" y2="20.32" width="0.1524" layer="91"/>
<pinref part="CON_DSP" gate="G$1" pin="P6"/>
<wire x1="45.72" y1="25.4" x2="35.56" y2="25.4" width="0.1524" layer="91"/>
<junction x="45.72" y="25.4"/>
</segment>
</net>
<net name="3.3V" class="0">
<segment>
<pinref part="CON_MCU" gate="G$1" pin="3.3V"/>
<pinref part="IC2" gate="G$1" pin="VIO"/>
<wire x1="45.72" y1="55.88" x2="48.26" y2="55.88" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="S"/>
<wire x1="68.58" y1="68.58" x2="76.2" y2="68.58" width="0.1524" layer="91"/>
<wire x1="45.72" y1="58.42" x2="48.26" y2="58.42" width="0.1524" layer="91"/>
<wire x1="48.26" y1="58.42" x2="48.26" y2="55.88" width="0.1524" layer="91"/>
<junction x="48.26" y="55.88"/>
<wire x1="63.5" y1="63.5" x2="63.5" y2="55.88" width="0.1524" layer="91"/>
<wire x1="63.5" y1="55.88" x2="48.26" y2="55.88" width="0.1524" layer="91"/>
<wire x1="63.5" y1="63.5" x2="68.58" y2="63.5" width="0.1524" layer="91"/>
<wire x1="68.58" y1="63.5" x2="68.58" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PE5" class="0">
<segment>
<pinref part="CON_MCU" gate="G$1" pin="PE5"/>
<pinref part="IC2" gate="G$1" pin="TXD"/>
<wire x1="45.72" y1="63.5" x2="53.34" y2="63.5" width="0.1524" layer="91"/>
<wire x1="53.34" y1="63.5" x2="53.34" y2="48.26" width="0.1524" layer="91"/>
<wire x1="63.5" y1="43.18" x2="76.2" y2="43.18" width="0.1524" layer="91"/>
<wire x1="63.5" y1="43.18" x2="63.5" y2="48.26" width="0.1524" layer="91"/>
<wire x1="63.5" y1="48.26" x2="53.34" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PE4" class="0">
<segment>
<pinref part="CON_MCU" gate="G$1" pin="PE4"/>
<pinref part="IC2" gate="G$1" pin="RXD"/>
<wire x1="50.8" y1="50.8" x2="50.8" y2="60.96" width="0.1524" layer="91"/>
<wire x1="50.8" y1="60.96" x2="45.72" y2="60.96" width="0.1524" layer="91"/>
<wire x1="76.2" y1="48.26" x2="76.2" y2="50.8" width="0.1524" layer="91"/>
<wire x1="76.2" y1="50.8" x2="50.8" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="12V" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="VIN"/>
<wire x1="33.02" y1="88.9" x2="30.48" y2="88.9" width="0.1524" layer="91"/>
<wire x1="30.48" y1="88.9" x2="25.4" y2="88.9" width="0.1524" layer="91"/>
<wire x1="25.4" y1="81.28" x2="25.4" y2="88.9" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="ANODE"/>
<junction x="30.48" y="88.9"/>
<wire x1="17.78" y1="81.28" x2="25.4" y2="81.28" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="VDD"/>
<wire x1="119.38" y1="22.86" x2="119.38" y2="20.32" width="0.1524" layer="91"/>
<wire x1="119.38" y1="20.32" x2="121.92" y2="20.32" width="0.1524" layer="91"/>
<pinref part="IC5" gate="G$1" pin="S"/>
<pinref part="C11" gate="G$1" pin="P$2"/>
<pinref part="IC4" gate="G$1" pin="IN"/>
<wire x1="121.92" y1="17.78" x2="119.38" y2="17.78" width="0.1524" layer="91"/>
<wire x1="119.38" y1="17.78" x2="119.38" y2="20.32" width="0.1524" layer="91"/>
<junction x="119.38" y="20.32"/>
<wire x1="17.78" y1="81.28" x2="17.78" y2="78.74" width="0.1524" layer="91"/>
<wire x1="17.78" y1="78.74" x2="17.78" y2="73.66" width="0.1524" layer="91"/>
<wire x1="154.94" y1="22.86" x2="149.86" y2="22.86" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="P$2"/>
<wire x1="119.38" y1="20.32" x2="114.3" y2="20.32" width="0.1524" layer="91"/>
<junction x="114.3" y="20.32"/>
<wire x1="149.86" y1="22.86" x2="119.38" y2="22.86" width="0.1524" layer="91"/>
<junction x="149.86" y="22.86"/>
<pinref part="CON_CAN" gate="G$1" pin="P5"/>
<wire x1="17.78" y1="73.66" x2="71.12" y2="73.66" width="0.1524" layer="91"/>
<wire x1="71.12" y1="73.66" x2="71.12" y2="17.78" width="0.1524" layer="91"/>
<wire x1="17.78" y1="78.74" x2="15.24" y2="78.74" width="0.1524" layer="91"/>
<junction x="17.78" y="78.74"/>
<wire x1="71.12" y1="17.78" x2="106.68" y2="17.78" width="0.1524" layer="91"/>
<wire x1="106.68" y1="17.78" x2="106.68" y2="20.32" width="0.1524" layer="91"/>
<wire x1="106.68" y1="20.32" x2="114.3" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="V_SW" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="VSW"/>
<pinref part="D1" gate="G$1" pin="CATHODE"/>
<pinref part="L1" gate="G$1" pin="P$1"/>
<wire x1="53.34" y1="86.36" x2="60.96" y2="86.36" width="0.1524" layer="91"/>
<junction x="53.34" y="86.36"/>
<pinref part="C2" gate="G$1" pin="P$2"/>
<wire x1="60.96" y1="88.9" x2="60.96" y2="86.36" width="0.1524" layer="91"/>
<junction x="60.96" y="86.36"/>
</segment>
</net>
<net name="V_CB" class="0">
<segment>
<pinref part="C2" gate="G$1" pin="P$1"/>
<pinref part="IC1" gate="G$1" pin="CB"/>
</segment>
</net>
<net name="CAN-L" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="CANL"/>
<wire x1="25.4" y1="58.42" x2="17.78" y2="58.42" width="0.1524" layer="91"/>
<pinref part="CON_CAN" gate="G$1" pin="P1"/>
<wire x1="15.24" y1="68.58" x2="17.78" y2="68.58" width="0.1524" layer="91"/>
<wire x1="17.78" y1="68.58" x2="17.78" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CAN-H" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="CANH"/>
<wire x1="25.4" y1="60.96" x2="20.32" y2="60.96" width="0.1524" layer="91"/>
<pinref part="CON_CAN" gate="G$1" pin="P2"/>
<wire x1="20.32" y1="60.96" x2="20.32" y2="71.12" width="0.1524" layer="91"/>
<wire x1="20.32" y1="71.12" x2="15.24" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="VCC"/>
<pinref part="C4" gate="G$1" pin="P$2"/>
<wire x1="45.72" y1="66.04" x2="55.88" y2="66.04" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="P$2"/>
<pinref part="CON_MCU" gate="G$1" pin="VBUS"/>
<wire x1="76.2" y1="71.12" x2="78.74" y2="71.12" width="0.1524" layer="91"/>
<wire x1="76.2" y1="86.36" x2="76.2" y2="71.12" width="0.1524" layer="91"/>
<wire x1="71.12" y1="86.36" x2="76.2" y2="86.36" width="0.1524" layer="91"/>
<junction x="71.12" y="86.36"/>
<pinref part="IC1" gate="G$1" pin="FB"/>
<wire x1="53.34" y1="91.44" x2="53.34" y2="93.98" width="0.1524" layer="91"/>
<wire x1="53.34" y1="93.98" x2="71.12" y2="93.98" width="0.1524" layer="91"/>
<wire x1="71.12" y1="93.98" x2="71.12" y2="86.36" width="0.1524" layer="91"/>
<junction x="55.88" y="66.04"/>
<junction x="76.2" y="71.12"/>
<pinref part="C3" gate="G$1" pin="ANODE"/>
<wire x1="66.04" y1="71.12" x2="76.2" y2="71.12" width="0.1524" layer="91"/>
<wire x1="55.88" y1="66.04" x2="66.04" y2="66.04" width="0.1524" layer="91"/>
<wire x1="66.04" y1="66.04" x2="66.04" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="GND"/>
<wire x1="45.72" y1="53.34" x2="55.88" y2="53.34" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="P$1"/>
<wire x1="55.88" y1="53.34" x2="55.88" y2="58.42" width="0.1524" layer="91"/>
<wire x1="45.72" y1="12.7" x2="50.8" y2="12.7" width="0.1524" layer="91"/>
<wire x1="73.66" y1="66.04" x2="78.74" y2="66.04" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="P$1"/>
<junction x="50.8" y="12.7"/>
<pinref part="C6" gate="G$1" pin="P$1"/>
<pinref part="CON_MCU" gate="G$1" pin="GND"/>
<junction x="55.88" y="53.34"/>
<pinref part="D1" gate="G$1" pin="ANODE"/>
<wire x1="71.12" y1="78.74" x2="53.34" y2="78.74" width="0.1524" layer="91"/>
<wire x1="53.34" y1="78.74" x2="33.02" y2="78.74" width="0.1524" layer="91"/>
<junction x="53.34" y="78.74"/>
<pinref part="IC1" gate="G$1" pin="GND"/>
<wire x1="33.02" y1="86.36" x2="33.02" y2="78.74" width="0.1524" layer="91"/>
<wire x1="33.02" y1="78.74" x2="30.48" y2="78.74" width="0.1524" layer="91"/>
<junction x="33.02" y="78.74"/>
<wire x1="30.48" y1="81.28" x2="30.48" y2="78.74" width="0.1524" layer="91"/>
<junction x="45.72" y="12.7"/>
<pinref part="C1" gate="G$1" pin="CATHODE"/>
<pinref part="C3" gate="G$1" pin="CATHODE"/>
<junction x="71.12" y="78.74"/>
<wire x1="38.1" y1="12.7" x2="45.72" y2="12.7" width="0.1524" layer="91"/>
<junction x="71.12" y="78.74"/>
<pinref part="CON_ENC" gate="G$1" pin="P1"/>
<junction x="30.48" y="78.74"/>
<pinref part="IC4" gate="G$1" pin="VSS"/>
<pinref part="C11" gate="G$1" pin="P$1"/>
<pinref part="C7" gate="G$1" pin="P$1"/>
<junction x="114.3" y="12.7"/>
<wire x1="114.3" y1="12.7" x2="114.3" y2="10.16" width="0.1524" layer="91"/>
<wire x1="114.3" y1="12.7" x2="121.92" y2="12.7" width="0.1524" layer="91"/>
<wire x1="114.3" y1="10.16" x2="154.94" y2="10.16" width="0.1524" layer="91"/>
<wire x1="154.94" y1="10.16" x2="162.56" y2="10.16" width="0.1524" layer="91"/>
<wire x1="154.94" y1="15.24" x2="154.94" y2="10.16" width="0.1524" layer="91"/>
<junction x="154.94" y="10.16"/>
<wire x1="114.3" y1="12.7" x2="104.14" y2="12.7" width="0.1524" layer="91"/>
<pinref part="CON_DSP" gate="G$1" pin="P1"/>
<wire x1="104.14" y1="12.7" x2="73.66" y2="12.7" width="0.1524" layer="91"/>
<wire x1="38.1" y1="12.7" x2="35.56" y2="12.7" width="0.1524" layer="91"/>
<pinref part="CON_DSP" gate="G$1" pin="P2"/>
<wire x1="38.1" y1="12.7" x2="38.1" y2="15.24" width="0.1524" layer="91"/>
<wire x1="38.1" y1="15.24" x2="35.56" y2="15.24" width="0.1524" layer="91"/>
<junction x="38.1" y="12.7"/>
<pinref part="CON_WAKE" gate="G$1" pin="P$1"/>
<pinref part="CON_CAN" gate="G$1" pin="P4"/>
<wire x1="104.14" y1="12.7" x2="104.14" y2="38.1" width="0.1524" layer="91"/>
<wire x1="104.14" y1="38.1" x2="111.76" y2="38.1" width="0.1524" layer="91"/>
<wire x1="73.66" y1="12.7" x2="73.66" y2="53.34" width="0.1524" layer="91"/>
<junction x="73.66" y="66.04"/>
<wire x1="73.66" y1="53.34" x2="73.66" y2="66.04" width="0.1524" layer="91"/>
<wire x1="71.12" y1="78.74" x2="73.66" y2="78.74" width="0.1524" layer="91"/>
<wire x1="73.66" y1="78.74" x2="73.66" y2="66.04" width="0.1524" layer="91"/>
<wire x1="55.88" y1="53.34" x2="73.66" y2="53.34" width="0.1524" layer="91"/>
<junction x="73.66" y="53.34"/>
<wire x1="15.24" y1="76.2" x2="20.32" y2="76.2" width="0.1524" layer="91"/>
<wire x1="20.32" y1="76.2" x2="20.32" y2="78.74" width="0.1524" layer="91"/>
<wire x1="20.32" y1="78.74" x2="30.48" y2="78.74" width="0.1524" layer="91"/>
<wire x1="50.8" y1="12.7" x2="73.66" y2="12.7" width="0.1524" layer="91"/>
<junction x="73.66" y="12.7"/>
<junction x="104.14" y="12.7"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="CON_ENC" gate="G$1" pin="P4"/>
<pinref part="CON_MCU" gate="G$1" pin="PA2"/>
<wire x1="109.22" y1="30.48" x2="111.76" y2="30.48" width="0.1524" layer="91"/>
<wire x1="109.22" y1="30.48" x2="109.22" y2="27.94" width="0.1524" layer="91"/>
<wire x1="101.6" y1="25.4" x2="101.6" y2="27.94" width="0.1524" layer="91"/>
<wire x1="101.6" y1="27.94" x2="109.22" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="CON_ENC" gate="G$1" pin="P3"/>
<pinref part="CON_MCU" gate="G$1" pin="PA3"/>
<wire x1="101.6" y1="33.02" x2="111.76" y2="33.02" width="0.1524" layer="91"/>
<wire x1="101.6" y1="30.48" x2="101.6" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="CON_MCU" gate="G$1" pin="PA4"/>
<pinref part="CON_ENC" gate="G$1" pin="P2"/>
<wire x1="111.76" y1="35.56" x2="101.6" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="P_OUT"/>
<pinref part="R1" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="N_OUT"/>
<pinref part="R1" gate="G$1" pin="P$2"/>
<wire x1="132.08" y1="15.24" x2="142.24" y2="15.24" width="0.1524" layer="91"/>
<wire x1="142.24" y1="15.24" x2="142.24" y2="17.78" width="0.1524" layer="91"/>
<junction x="142.24" y="17.78"/>
<pinref part="IC5" gate="G$1" pin="G"/>
</segment>
</net>
<net name="WAKE" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="D"/>
<wire x1="162.56" y1="12.7" x2="149.86" y2="12.7" width="0.1524" layer="91"/>
<pinref part="CON_WAKE" gate="G$1" pin="P$2"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="INB"/>
<wire x1="121.92" y1="15.24" x2="116.84" y2="15.24" width="0.1524" layer="91"/>
<wire x1="116.84" y1="15.24" x2="116.84" y2="22.86" width="0.1524" layer="91"/>
<wire x1="116.84" y1="22.86" x2="106.68" y2="22.86" width="0.1524" layer="91"/>
<pinref part="CON_MCU" gate="G$1" pin="PC6"/>
<wire x1="106.68" y1="22.86" x2="106.68" y2="43.18" width="0.1524" layer="91"/>
<wire x1="106.68" y1="43.18" x2="99.06" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="P$2"/>
<pinref part="CON_MCU" gate="G$1" pin="PD1"/>
<wire x1="78.74" y1="55.88" x2="68.58" y2="55.88" width="0.1524" layer="91"/>
<wire x1="68.58" y1="55.88" x2="68.58" y2="35.56" width="0.1524" layer="91"/>
<wire x1="68.58" y1="35.56" x2="50.8" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="P$2"/>
<wire x1="45.72" y1="38.1" x2="45.72" y2="35.56" width="0.1524" layer="91"/>
<pinref part="CON_MCU" gate="G$1" pin="PD0"/>
<wire x1="45.72" y1="38.1" x2="66.04" y2="38.1" width="0.1524" layer="91"/>
<wire x1="66.04" y1="38.1" x2="66.04" y2="60.96" width="0.1524" layer="91"/>
<wire x1="66.04" y1="60.96" x2="78.74" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
