// lladoware_system_config.h
// 
// System configuration parameters for TI TM4C123GXL using Keil v5
// 
// This file is part of lladoware v1.0
// Travis Llado, travis@travisllado.com
// Last modified 2020-04-21

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "tm4c123gh6pm.h"

////////////////////////////////////////////////////////////////////////////////
// Constants

#define SYS_FREQ    80000000    // Hz
#define PWM_DIV     2           // System Clock ticks per PWM Clock tick

////////////////////////////////////////////////////////////////////////////////
// End of file
