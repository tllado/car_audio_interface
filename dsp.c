// dsp.c
// 
// Car audio control interface for TI TM4C123GXL using Keil v5
// Program generates analog voltage control signals for Audiotec Fischer DSP 
// and modulates those voltages based on button input from user. Eventually 
// button input from user will be replaced with CANBUS input from car.
// 
// This file contains code that manages the operation of the PWM outputs that 
// drive indicator LEDs and analog voltage outputs.
// 
// This file is part of car_audio_interface v0.1
// Travis Llado, travis@travisllado.com
// Last modified 2020-04-22

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "tm4c123gh6pm.h"
#include "dsp.h"
#include "lladoware_pins.h"
#include "lladoware_timing.h"

////////////////////////////////////////////////////////////////////////////////
// Internal Prototypes

void update_bass_level(void);
void update_volume_level(void);

////////////////////////////////////////////////////////////////////////////////
// Global Variables

int32_t bass_level = BASS_INIT;
int32_t volume_level = VOLUME_INIT;

////////////////////////////////////////////////////////////////////////////////
// dsp_bass_lower()
// Decreases bass level one tick

void dsp_bass_lower(void){
    if(bass_level == BASS_MIN){
        // do nothing
    }
    else{
        if(bass_level <= BASS_MIN + 1)
            bass_level = BASS_MIN;
        else
            bass_level--;
        update_bass_level();
    }
}

////////////////////////////////////////////////////////////////////////////////
// dsp_bass_raise()
// Increases bass level one tick

void dsp_bass_raise(void){
    if(bass_level == BASS_MAX){
        // do nothing
    }
    else{
        if(bass_level >= BASS_MAX - 1)
            bass_level = BASS_MAX;
        else
            bass_level++;
        update_bass_level();
    }
}

////////////////////////////////////////////////////////////////////////////////
// dsp_bass_reset()
// Sets bass level to initialization value

void dsp_bass_reset(void){
    bass_level = BASS_INIT;
    update_bass_level();
}

////////////////////////////////////////////////////////////////////////////////
// dsp_encoder_handler()
// Updates bass level according to encoder motion

void dsp_encoder_handler(void){
    static uint32_t st1;    // state current
    static uint32_t st0;    // state previous
    static int32_t total_change = 0; 
    const int32_t step_size = 4;       // encoder moves too fast, decrease by this factor

    st1 = (read_pin(PIN_BASS_ENC_A) << 1) + read_pin(PIN_BASS_ENC_B);

    // clunky but simple way of processing Gray Code (0,1,3,2,0,...)
    if((st0 == 0 & st1 == 1) | (st0 == 1 & st1 == 3) | (st0 == 3 & st1 == 2) | (st0 == 2 & st1 == 0))
        total_change++;
    else
        total_change--;

    if(total_change >= step_size){
        total_change = 0;
        dsp_bass_raise();
    }
    else if(total_change <= -step_size){
        total_change = 0;
        dsp_bass_lower();
    }

    st0 = st1;
}

////////////////////////////////////////////////////////////////////////////////
// dsp_init()
// Initializes PWM signals and channel levels

void dsp_init(void){
	onboard_leds_init();
    init_button(PIN_BASS_RESET, 0, &dsp_bass_reset);
    init_quad_encoder(PIN_BASS_ENC_A, PIN_BASS_ENC_B, 1, &dsp_encoder_handler);
    
    init_pin_output(PIN_VOL_SGNL,  PWM_PERIOD);
    init_pin_output(PIN_BASS_SGNL, PWM_PERIOD);

    dsp_bass_reset();
    dsp_volume_reset();
}

////////////////////////////////////////////////////////////////////////////////
// dsp_volume_lower()
// Decreases volume level one tick

void dsp_volume_lower(void){
    if(volume_level == VOLUME_MIN){
        // do nothing
    }
    else{
        if(volume_level <= VOLUME_MIN + 1)
            volume_level = VOLUME_MIN;
        else
            volume_level--;
        update_volume_level();
    }
}

////////////////////////////////////////////////////////////////////////////////
// dsp_volume_lower_by()
// Decreases volume level by specified number of ticks

void dsp_volume_lower_by(uint32_t num_ticks){
    if(volume_level == VOLUME_MIN){
        // do nothing
    }
    else{
        if(volume_level  <= (VOLUME_MIN + num_ticks))
            volume_level = VOLUME_MIN;
        else
            volume_level -= num_ticks;
        update_volume_level();
    }
}

////////////////////////////////////////////////////////////////////////////////
// dsp_volume_raise()
// Increases volume level one tick

void dsp_volume_raise(void){
    if(volume_level == VOLUME_MAX){
        // do nothing
    }
    else{
        if(volume_level >= VOLUME_MAX - 1)
            volume_level = VOLUME_MAX;
        else
            volume_level++;
        update_volume_level();
    }
}

////////////////////////////////////////////////////////////////////////////////
// dsp_volume_raise_by()
// Increases volume level by specified number of ticks

void dsp_volume_raise_by(uint32_t num_ticks){
    if(volume_level == VOLUME_MAX){
        // do nothing
    }
    else{
        if(volume_level >= VOLUME_MAX - num_ticks)
            volume_level = VOLUME_MAX;
        else
            volume_level += num_ticks;
        update_volume_level();
    }
}

////////////////////////////////////////////////////////////////////////////////
// dsp_volume_reset()
// Sets volume level to initialization value

void dsp_volume_reset(void){
    volume_level = VOLUME_INIT;
    update_volume_level();
}

////////////////////////////////////////////////////////////////////////////////
// update_bass()
// Sends new bass level down to PWM control

void update_bass_level(void){
    if(bass_level > BASS_MAX)
        bass_level = BASS_MAX;
    else if(bass_level < BASS_MIN)
        bass_level = BASS_MIN;

    // update analog voltage immediately
    uint32_t duty = PWM_PERIOD*bass_level/VOLUME_MAX;
    update_pin(PIN_BASS_SGNL, duty);
    onboard_leds_blue_update(duty);
}

////////////////////////////////////////////////////////////////////////////////
// update_volume()
// Sends new volume level down to PWM control

void update_volume_level(void){
    if(volume_level > VOLUME_MAX)
        volume_level = VOLUME_MAX;
    else if(volume_level < VOLUME_MIN)
        volume_level = VOLUME_MIN;

    // update analog voltage immediately
    uint32_t duty = PWM_PERIOD*volume_level/VOLUME_MAX;
    update_pin(PIN_VOL_SGNL, duty);
    onboard_leds_green_update(duty);
}

////////////////////////////////////////////////////////////////////////////////
// End of file
