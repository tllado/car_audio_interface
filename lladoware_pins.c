// lladoware_pins.c
// 
// Onboard LED control for TI TM4C123GXL using Keil v5
// A simple library that initializes and updates all GPIO and PWM output pins 
// on a TI TM4C123GXL microcontroller
// 
// This file is part of lladoware v1.0
// Travis Llado, travis@travisllado.com
// Last modified 2020-04-22

////////////////////////////////////////////////////////////////////////////////
// Dependencies
#include "lladoware_system_config.h"
#include "lladoware_pins.h"
#include "lladoware_pin_regs.h"
#include "lladoware_timing.h"

// from startup.s
void DisableInterrupts(void);
void EnableInterrupts(void);

////////////////////////////////////////////////////////////////////////////////
// Internal Prototypes

void debounce(void);
uint32_t init_pin_output_gpio(uint32_t pin_index);
uint32_t init_pin_output_pwm(uint32_t pin_index, uint32_t period);
uint32_t update_pin_gpio(uint32_t pin_index, uint32_t command);
uint32_t update_pin_pwm(uint32_t pin_index, uint32_t command);

////////////////////////////////////////////////////////////////////////////////
// Global Variables

uint32_t global_pwm_period[NUM_PINS];
void (*global_nvic_task[NUM_PINS])(void);

////////////////////////////////////////////////////////////////////////////////
// buttons_interrupt_handler()
// Generic interrupt handler for button presses on any GPIO pin header

void button_interrupt_handler(uint32_t pin_index_start, uint32_t pin_index_end){
    for(uint32_t pin_index = pin_index_start; pin_index <= pin_index_end; pin_index++){
        if(*GPIO_RIS_REG[pin_index] & (0x01 << PIN_NUM[pin_index])){
            *GPIO_ICR_REG[pin_index] = (0x01 << PIN_NUM[pin_index]);
            global_nvic_task[pin_index]();
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// debounce()
// A very primitive debounce function. Just wastes some time before clearing
// button interrupt.

void debounce(void){
    wait_us(DEBOUNCE_LENGTH);
}

////////////////////////////////////////////////////////////////////////////////
// GPIOPortA_Handler()
// Decides what action to take for any interrupt on Port A

void GPIOPortA_Handler(void){
    debounce();
    button_interrupt_handler(PA2, PA7);
}

////////////////////////////////////////////////////////////////////////////////
// GPIOPortB_Handler()
// Decides what action to take for any interrupt on Port B

void GPIOPortB_Handler(void){
    debounce();
    button_interrupt_handler(PB0, PB7);
}

////////////////////////////////////////////////////////////////////////////////
// GPIOPortC_Handler()
// Decides what action to take for any interrupt on Port C

void GPIOPortC_Handler(void){
    debounce();
    button_interrupt_handler(PC4, PC7);
}

////////////////////////////////////////////////////////////////////////////////
// GPIOPortD_Handler()
// Decides what action to take for any interrupt on Port D

void GPIOPortD_Handler(void){
    debounce();
    button_interrupt_handler(PD0, PD3);
    button_interrupt_handler(PD6, PD7);
}

////////////////////////////////////////////////////////////////////////////////
// GPIOPortE_Handler()
// Decides what action to take for any interrupt on Port E

void GPIOPortE_Handler(void){
    debounce();
    button_interrupt_handler(PE0, PE5);
}

////////////////////////////////////////////////////////////////////////////////
// GPIOPortF_Handler()
// Decides what action to take for any interrupt on Port F

void GPIOPortF_Handler(void){
    debounce();
    button_interrupt_handler(PF0, PF4);
}

////////////////////////////////////////////////////////////////////////////////
// init_button()
// 
// Initializes hardware and software associated with Launchpad's two onboard
// switches. Takes input of two function pointers that are referenced when
// GPIO Port F interrupt handler is executed.

uint32_t init_button(uint32_t pin_index, uint32_t priority, void (*task)(void)){
    // check argument validity
    if(priority > 7)
        return 1;

    // Configure pin as GPIO input
    init_pin_input_gpio(pin_index);

    // Configure interrupt
    DisableInterrupts();
        *GPIO_IS_REG[pin_index]  &= ~(0x01 << PIN_NUM[pin_index]);  // make pin edge sensitive
        *GPIO_IBE_REG[pin_index] &= ~(0x01 << PIN_NUM[pin_index]);  // make sensitive to one edge
        *GPIO_IEV_REG[pin_index] &= ~(0x01 << PIN_NUM[pin_index]);  // make sensitive to falling edge
        *GPIO_ICR_REG[pin_index] |=  (0x01 << PIN_NUM[pin_index]);  // set interrupt clear
        *GPIO_IM_REG[pin_index]  |=  (0x01 << PIN_NUM[pin_index]);  // arm interrupt

        *NVIC_PRIO_REG[pin_index] &= ~(0x0E << NVIC_PRIO_OFFSET[pin_index]);        // clear interrupt priority
        *NVIC_PRIO_REG[pin_index] |=  (priority << NVIC_PRIO_OFFSET[pin_index]);    // set interrupt priority
        NVIC_EN0_R                |=   NVIC_ENBL_REG[pin_index];                    // enable interrupt
    EnableInterrupts();

    // Configure interrupt tasks
    global_nvic_task[pin_index] = task;

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
// init_pin_input_gpio()
// Initializes one pin as GPIO and sets it to LOW

uint32_t init_pin_input_gpio(uint32_t pin_index){

    DisableInterrupts();
        SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO[pin_index];            // Enable GPIO clock
        while((SYSCTL_PRGPIO_R & SYSCTL_PRGPIO[pin_index]) == 0) {} // Wait until peripheral ready

        *GPIO_LOCK_REG[pin_index]  =   GPIO_LOCK_KEY;                       // unlock GPIO Commit Reg
        *GPIO_CR_REG[pin_index]    |=  (0x01 << PIN_NUM[pin_index]);        // Enable Commits

        *GPIO_AFSEL_REG[pin_index] &= ~(0x01 << PIN_NUM[pin_index]);        // disable alt functions
        *GPIO_AMSEL_REG[pin_index] &= ~(0x01 << PIN_NUM[pin_index]);        // disable analog function
        *GPIO_DEN_REG[pin_index]   |=  (0x01 << PIN_NUM[pin_index]);        // enable digital I/O
        *GPIO_DIR_REG[pin_index]   &= ~(0x01 << PIN_NUM[pin_index]);        // set GPIO direction to input
        *GPIO_PCTL_REG[pin_index]  &= ~(0x0F << (4*PIN_NUM[pin_index]));    // enable GPIO function
        *GPIO_PUR_REG[pin_index]   |=  (0x01 << PIN_NUM[pin_index]);        // enable weak pull-up
    EnableInterrupts();

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
// init_pin_input()
// Initializes one pin as either PWM and GPIO or as just GPIO
// If desired to use pin as GPIO or with default PWM frequency of 1kHz, then 
// period should be set as 0

uint32_t init_pin_output(uint32_t pin_index, uint32_t period){
    if(PWM_AF_NUM[pin_index] == INVALID)
        return init_pin_output_gpio(pin_index);
    else
        return init_pin_output_pwm(pin_index, period);
}

////////////////////////////////////////////////////////////////////////////////
// init_pin_output_gpio()
// Initializes one pin as GPIO and sets it to LOW

uint32_t init_pin_output_gpio(uint32_t pin_index){
    DisableInterrupts();
        SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO[pin_index];            // Enable GPIO clock
        while((SYSCTL_PRGPIO_R & SYSCTL_PRGPIO[pin_index]) == 0) {} // Wait until peripheral ready

        *GPIO_LOCK_REG[pin_index]  =   GPIO_LOCK_KEY;                       // unlock GPIO Commit Reg
        *GPIO_CR_REG[pin_index]    |=  (0x01 << PIN_NUM[pin_index]);        // Enable Commits

        *GPIO_AFSEL_REG[pin_index] &= ~(0x01 << PIN_NUM[pin_index]);        // disable alt functions
        *GPIO_AMSEL_REG[pin_index] &= ~(0x01 << PIN_NUM[pin_index]);        // disable analog function
        *GPIO_DEN_REG[pin_index]   |=  (0x01 << PIN_NUM[pin_index]);        // enable digital I/O
        *GPIO_DIR_REG[pin_index]   |=  (0x01 << PIN_NUM[pin_index]);        // set GPIO direction to out
        *GPIO_PCTL_REG[pin_index]  &= ~(0x0F << (4*PIN_NUM[pin_index]));    // enable GPIO function
    EnableInterrupts();

    *GPIO_DATA_BITS_REG[pin_index] &= ~(0x01 << PIN_NUM[pin_index]);    // Initialize pin to LOW

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
// init_pin_output_pwm()
// Initializes one pin as PWM, prepares it for quick switching to GPIO if 
// necessary, and sets duty to 0

uint32_t init_pin_output_pwm(uint32_t pin_index, uint32_t period){
    if(period > PWM_PERIOD_MAX)
        return 1;
    if(period == 0)
        period = PWM_PERIOD_DEFAULT;

    global_pwm_period[pin_index] = period;

    DisableInterrupts();
        SYSCTL_RCGCPWM_R  |= SYSCTL_RCGCPWM[pin_index];             // Initialize PWM clock
        SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO[pin_index];            // Initialize GPIO clock
        while((SYSCTL_PRGPIO_R & SYSCTL_PRGPIO[pin_index]) == 0) {} // Wait until peripheral ready
        SYSCTL_RCC_R |= SYSCTL_RCC_USEPWMDIV;                       // Set PWM clock w.r.t. system clock
        SYSCTL_RCC_R &= ~SYSCTL_RCC_PWMDIV_M;
        SYSCTL_RCC_R += SYSCTL_RCC_PWMDIV_2;

        *GPIO_LOCK_REG[pin_index]  =   GPIO_LOCK_KEY;                       // unlock GPIO Commit Reg
        *GPIO_CR_REG[pin_index]    |=  (0x01 << PIN_NUM[pin_index]);        // Enable Commits

        *GPIO_AFSEL_REG[pin_index] |=  (0x01 << PIN_NUM[pin_index]);                        // enable alt functions
        *GPIO_AMSEL_REG[pin_index] &= ~(0x01 << PIN_NUM[pin_index]);                        // disable analog function
        *GPIO_DEN_REG[pin_index]   |=  (0x01 << PIN_NUM[pin_index]);                        // enable digital I/O
        *GPIO_DIR_REG[pin_index]   |=  (0x01 << PIN_NUM[pin_index]);                        // set GPIO direction to out
        *GPIO_PCTL_REG[pin_index]  &= ~(0x0F << (4*PIN_NUM[pin_index]));                    // disable other alt functions
        *GPIO_PCTL_REG[pin_index]  |=  (PWM_AF_NUM[pin_index] << (4*PIN_NUM[pin_index]));   // enable PWM function

        *PWM_CTL_REG[pin_index]   = 0;                          // set countdown mode
        *PWM_GEN_REG[pin_index]   = PWM_TRGR[pin_index];        // define signal triggers
        *PWM_LOAD_REG[pin_index]  = period;                     // set counter reset values (period)
        *PWM_CMP_REG[pin_index]   = 0;                          // init comparator values (duty)
        *PWM_CTL_REG[pin_index]  |= PWM_BLK_ENBL[pin_index];    // enable block
        *PWM_ENBL_REG[pin_index] |= PWM_MOD_ENBL[pin_index];    // enable module
    EnableInterrupts();

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
// init_quad_encoder()
// 
// Initializes handlers for quadrature encoder that connects to two GPIO input 
// pins

uint32_t init_quad_encoder(uint32_t pin_index_a, uint32_t pin_index_b, uint32_t priority, void (*task)(void)){
    // check argument validity
    if(priority > 7)
        return 1;

    // Configure pins as GPIO input
    init_pin_input_gpio(pin_index_a);
    init_pin_input_gpio(pin_index_b);

    // Configure interrupts
    DisableInterrupts();
        *GPIO_IM_REG[pin_index_a]  &= ~(0x01 << PIN_NUM[pin_index_a]);  // Disable Interrupts
        *GPIO_IM_REG[pin_index_b]  &= ~(0x01 << PIN_NUM[pin_index_b]);
        *GPIO_IS_REG[pin_index_a]  &= ~(0x01 << PIN_NUM[pin_index_a]);  // enable Interrupt Sensing
        *GPIO_IS_REG[pin_index_b]  &= ~(0x01 << PIN_NUM[pin_index_b]);
        *GPIO_IBE_REG[pin_index_a] |=  (0x01 << PIN_NUM[pin_index_a]);  // enable Interrupt Both Edges
        *GPIO_IBE_REG[pin_index_b] |=  (0x01 << PIN_NUM[pin_index_b]);

        *NVIC_PRIO_REG[pin_index_a] &= ~(0x0E << NVIC_PRIO_OFFSET[pin_index_a]);        // clear interrupt priority
        *NVIC_PRIO_REG[pin_index_b] &= ~(0x0E << NVIC_PRIO_OFFSET[pin_index_b]);
        *NVIC_PRIO_REG[pin_index_a] |=  (priority << NVIC_PRIO_OFFSET[pin_index_a]);    // set interrupt priority
        *NVIC_PRIO_REG[pin_index_b] |=  (priority << NVIC_PRIO_OFFSET[pin_index_b]);
        NVIC_EN0_R                  |=   NVIC_ENBL_REG[pin_index_a];                    // enable interrupt controller
        NVIC_EN0_R                  |=   NVIC_ENBL_REG[pin_index_b];

        *GPIO_ICR_REG[pin_index_a] |=  (0x01 << PIN_NUM[pin_index_a]);  // set Interrupt Clear
        *GPIO_ICR_REG[pin_index_b] |=  (0x01 << PIN_NUM[pin_index_b]);
        *GPIO_IM_REG[pin_index_a]  |=  (0x01 << PIN_NUM[pin_index_a]);  // Enable Interrupts
        *GPIO_IM_REG[pin_index_b]  |=  (0x01 << PIN_NUM[pin_index_b]);
    EnableInterrupts();

    // Configure interrupt tasks
    global_nvic_task[pin_index_a] = task;
    global_nvic_task[pin_index_b] = task;

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
// onboard_leds_blue_update()
// Checks then updates duty cycle for blue LED

void onboard_leds_blue_update(uint32_t command){
    update_pin(PF2, command);
}

////////////////////////////////////////////////////////////////////////////////
// onboard_leds_green_update()
// Checks then updates duty cycle for green LED

void onboard_leds_green_update(uint32_t command){
    update_pin(PF3, command);
}

////////////////////////////////////////////////////////////////////////////////
// onboard_leds_init()
// Initialize PF1-3 as PWM output

void onboard_leds_init(void) {
    init_pin_output(PF1, LED_PERIOD_DEFAULT);
    init_pin_output(PF2, LED_PERIOD_DEFAULT);
    init_pin_output(PF3, LED_PERIOD_DEFAULT);
}

////////////////////////////////////////////////////////////////////////////////
// onboard_leds_red_update()
// Checks then updates duty cycle for red LED

void onboard_leds_red_update(uint32_t command){
    update_pin(PF1, command);
}

////////////////////////////////////////////////////////////////////////////////
// read_pin()
// Returns current input pin value

uint32_t read_pin(uint32_t pin_index){
    return (*GPIO_DATA_REG[pin_index] & (0x01 << PIN_NUM[pin_index])) >> PIN_NUM[pin_index];
}

////////////////////////////////////////////////////////////////////////////////
// update_pin()
// Updates GPIO state or PWM duty

uint32_t update_pin(uint32_t pin_index, uint32_t command){
    if((command == HIGH) || (command == LOW))
        return update_pin_gpio(pin_index, command);
    else
        return update_pin_pwm(pin_index, command);
}

////////////////////////////////////////////////////////////////////////////////
// update_pin_input_gpio()
// Reinitializes pin as GPIO (faster than checking state) and updates state

uint32_t update_pin_gpio(uint32_t pin_index, uint32_t command){
    DisableInterrupts();
        *GPIO_AFSEL_REG[pin_index] &= ~(0x01 << PIN_NUM[pin_index]);    // disable alt functions
        *GPIO_PCTL_REG[pin_index] &= ~(0x0F << (4*PIN_NUM[pin_index])); // enable GPIO function
    EnableInterrupts();

    // Set new GPIO state
    if(command == HIGH)
        *GPIO_DATA_BITS_REG[pin_index] = (0x01 << PIN_NUM[pin_index]);
    else
        *GPIO_DATA_BITS_REG[pin_index] &= ~(0x01 << PIN_NUM[pin_index]);

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
// update_pin_input_pwn()
// Reinitializes pin as PWM (faster than checking state) and updates duty

uint32_t update_pin_pwm(uint32_t pin_index, uint32_t command){
    if((PWM_AF_NUM[pin_index] == INVALID) || (command >= global_pwm_period[pin_index]))
        return 1;

    DisableInterrupts();
        *GPIO_AFSEL_REG[pin_index] |= (0x01 << PIN_NUM[pin_index]);                     // enable alt functions
        *GPIO_PCTL_REG[pin_index] |= (PWM_AF_NUM[pin_index] << (4*PIN_NUM[pin_index])); // enable PWM function
    EnableInterrupts();

    *PWM_CMP_REG[pin_index] = command; // Set new PWM duty

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
// End of file
